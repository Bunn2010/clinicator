﻿using Clinicator.DBAccess;
using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.Controller
{
    
    /// <summary>
    /// The controller class that deals with the DB Access and delegate work to the DAL
    /// </summary>
    class PatientController
    {

        public PatientController()
        {

        }

        /// <summary>
        /// Communicates with Visit DAL to get a list of the visit dates
        /// </summary>
        /// <returns>List of visits</returns>
        public List<Visit> GetVisitDateList(int patientID)
        {
            return VisitDAL.GetVisitDateList(patientID);
        }

        /// <summary>
        /// Communicates with Visit DAL to get a specified visit
        /// </summary>
        /// <returns>Visit object</returns>
        public Visit GetVisit(int visitID)
        {
            return VisitDAL.GetVisit(visitID);
        }

        /// <summary>
        /// Inserts a new visit 
        /// </summary>
        /// <param name="visit">Visit object</param>
        /// <returns>Database ID of the new Visit</returns>
        public int CreateVisit(Visit visit)
        {
            return VisitDAL.CreateVisit(visit);
        }

        /// <summary>
        /// Communicates with Patient DAL to insert a new patient
        /// </summary>
        /// <param name="patient"></param>
        /// <returns>Database ID of the new patient</returns>
        public int CreatePatient(Patient patient)
        {
            return PatientDAL.CreatePatient(patient);
        }

        /// <summary>
        /// Communicates with PatientDAL to get a patient
        /// </summary>
        /// <param name="patientID"></param>
        /// <returns></returns>
        public Patient getPatientByID(int patientID)
        {
            return PatientDAL.getPatientByID(patientID);
        }

        /// <summary>
        /// Communicates with PatientDAL to find patients that match search attributes in the patient parameter
        /// </summary>
        /// <param name="patient"></param>
        /// <returns>List of patients that match</returns>
        public List<Patient> GetPatientsBySearchFields(Patient patient)
        {
            return PatientDAL.GetPatientsBySearchFields(patient);
        }

        /// <summary>
        /// Communicates with PatientDAL to update the patient registration information
        /// </summary>
        /// <param name="oldPatient"></param>
        /// <param name="newPatient"></param>
        /// <returns>True if patient was updated</returns>
        public bool UpdatePatient(Patient oldPatient, Patient newPatient)
        {
            return PatientDAL.UpdatePatient(oldPatient, newPatient);
        }

        /// <summary>
        /// Communicates with the VisitDAL to update the diagnosis information only
        /// </summary>
        /// <param name="oldVisit"></param>
        /// <param name="newVisit"></param>
        /// <returns>True if the diagnosis was updated</returns>
        public bool UpdateVisitDiagnosis(Visit oldVisit, Visit newVisit)
        {
            return VisitDAL.UpdateVisitDiagnosis(oldVisit, newVisit);
        }

        /// <summary>
        /// Communicates with the TestDAL to retrieve a list of active tests that can be order for a Patient
        /// </summary>
        /// <returns>List of Active Tests</returns>
        public List<Test> GetActiveTestListNotOrdered(int visitID)
        {
            return TestDAL.GetActiveTestListNotOrdered(visitID);
        }

        /// <summary>
        /// Communicates with the TestOrderDAL to insert a new TestOrder into the givent visit and test
        /// </summary>
        /// <param name="visitID">ID of visit associated with the test</param>
        /// <param name="testID">ID of test to be ordered</param>
        /// <returns>True if successful False if not</returns>
        public bool OrderTest(int visitID, int testID)
        {
            return TestOrderDAL.OrderTest(visitID, testID);
        }

        /// <summary>
        /// Retrieves a List of TestOrders for the given VisitID
        /// </summary>
        /// <param name="visitID">ID of current visit</param>
        /// <returns>List of TestOrders</returns>
        public List<TestOrder> GetTestOrderListByVisitID(int visitID)
        {
            return TestOrderDAL.GetTestOrderListByVisitID(visitID);
        }

        /// <summary>
        /// Communincates with the TestOrderDAL to update test information
        /// </summary>
        /// <param name="testOrder">testOrder cantaining information to update</param>
        /// <returns>True if successful false if not</returns>
        public bool UpdateTestOrder(TestOrder oldTestOrder, TestOrder newTestOrder)
        {
            return TestOrderDAL.UpdateTestOrder(oldTestOrder, newTestOrder);
        }

        /// <summary>
        /// Communicates with the TestOrderDAL to remove ordered tests
        /// </summary>
        /// <param name="testOrder"></param>
        /// <returns>True if the order was removed</returns>
        public bool DeleteTestOrder(TestOrder testOrder)
        {
            return TestOrderDAL.DeleteTestOrder(testOrder);
        }

        /// <summary>
        /// Communicates with the PatientDAL to determine if SSN already exists
        /// </summary>
        /// <param name="ssn">ssn to be checked for uniqueness</param>
        /// <returns>True if SSN is unique Fals if SSN already exists</returns>
        public bool IsPatientSSNUnique(int ssn)
        {
            return PatientDAL.IsPatientSSNUnique(ssn);
        }
    }
}
