﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clinicator.Model;
using Clinicator.DBAccess;

namespace Clinicator.Controller
{
    
    class StateController
    {
        /// <summary>
        /// The controller class that deals with StateDAL
        /// </summary>
        public StateController()
        {

        }

        /// <summary>
        /// Communicates with the StateDAL to retrieve a List of states
        /// </summary>
        /// <returns>List of State objects</returns>
        public List<State> GetStateList()
        {
            return StateDAL.GetStateList();
        }
    }
}
