﻿using Clinicator.DBAccess;
using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.Controller
{
    class AdminController
    {
        /// <summary>
        /// The controller class that deals with the DB Access and delegate work to the DAL
        /// </summary>
        public AdminController()
        {

        }

        /// <summary>
        /// Communicates with the DAL to Insert am Employee into the DB
        /// </summary>
        /// <param name="employee">Employee to be inserted into DB</param>
        /// <returns>True if successful False if not successful</returns>
        public int CreateEmployee(Employee employee)
        {
            return EmployeeDAL.CreateEmployee(employee);
        }

        /// <summary>
        /// Communicates with the DAL to Insert a UserAccount into the DB
        /// </summary>
        /// <param name="userAccount">UserAccount to be inserted into the DB</param>
        /// <returns>DB created ID</returns>
        public bool CreateUserAccount(UserAccount userAccount)
        {
            return UserAccountDAL.CreateUserAccount(userAccount);
        }

        /// <summary>
        /// Communicates with the DAL to Delete a UserAccount from the DB
        /// </summary>
        /// <param name="userAccount">UserAccount to be removed from DB</param>
        /// <returns>True if deleted</returns>
        public bool DeleteAccount(UserAccount userAccount)
        {
            return UserAccountDAL.DeleteAccount(userAccount);
        }

        /// <summary>
        /// Communicates with the DAL to get a specific employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Employee object</returns>
        public Employee GetEmployeeByID(int id)
        {
            return EmployeeDAL.GetEmployeeByID(id);
        }


        /// <summary>
        /// Communicates with the EmployeeDAL to get a list of employees by role.
        /// </summary>
        /// <returns></returns>
        public List<Employee> getActiveEmployeeListByRole(string role)
        {
            return EmployeeDAL.GetActiveEmployeeListByRole(role);
        }

        /// <summary>
        /// Communicates with the EmployeeDAL to get a list of employee roles.
        /// </summary>
        /// <returns></returns>
        public List<Employee> getEmployeeRoles()
        {
            return EmployeeDAL.GetEmployeeRoles();
        }

        /// <summary>
        /// Communicates with the EmployeeDAL to get a list of employees by search criteria.
        /// </summary>
        /// <returns>List of employees that match search criteria</returns>
        public List<Employee> GetEmployeesBySearchFields(Employee searchEmployee)
        {
            return EmployeeDAL.GetEmployeesBySearchFields(searchEmployee);
        }

        /// <summary>
        /// Communicates with the EmployeeDAL to update the given employee information
        /// </summary>
        /// <param name="oldEmployee">Original employee data</param>
        /// <param name="newEmployee">new employee data to replace the old data</param>
        /// <returns>True if successful False if not</returns>
        public bool UpdateEmployee(Employee oldEmployee, Employee newEmployee)
        {
            return EmployeeDAL.UpdateEmployee(oldEmployee, newEmployee);
        }

        /// <summary>
        /// Retrieves a list of all tests in DB
        /// </summary>
        /// <returns>List of all Tests</returns>
        public List<Test> GetTestList()
        {
            return TestDAL.GetTestList();
        }

        /// <summary>
        /// Communicates with the EmployeeDAL to Insert a new Test
        /// </summary>
        /// <param name="test"></param>
        /// <returns>int representing the testID</returns>
        public int CreateTest(Test test)
        {
            return TestDAL.CreateTest(test);
        }

        /// <summary>
        /// Communicates with TestDAL to determine if the name passed in is already in DB
        /// </summary>
        /// <param name="name"></param>
        /// <returns>True if there is already a test in the DB with this name</returns>
        public bool isTestNameDuplicate(string name)
        {
            return TestDAL.isTestNameDuplicate(name);
        }

        /// <summary>
        /// Communicates with the TestDAL to update the given test information 
        /// </summary>
        /// <param name="oldTest"></param>Original test data
        /// <param name="newTest"></param>New test data to replace the old test data
        /// <returns>True if successful False if not</returns>
        public bool updateTest(Test oldTest, Test newTest)
        {
            return TestDAL.updateTest(oldTest, newTest);
        }

        /// <summary>
        /// Communicates with the TestDAL to retrieve a list of tests that
        /// match the given search criteria
        /// </summary>
        /// <param name="test">test object containg the search criteria</param>
        /// <param name="shouldContainName">True if results should have names that contain the text in the search name</param>
        /// <returns>List of test that match search criteria</returns>
        public List<Test> searchByTestName(Test test, bool shouldContainName)
        {
            return TestDAL.searchByTestName(test, shouldContainName);
        }

        /// <summary>
        /// Communicates with the EmployeeDAL to determine if SSN already exists
        /// </summary>
        /// <param name="ssn">ssn to be checked for uniqueness</param>
        /// <returns>True if SSN is unique Fals if SSN already exists</returns>
        public bool IsEmployeeSSNUnique(int ssn)
        {
            return EmployeeDAL.IsEmployeeSSNUnique(ssn);
        }

        /// <summary>
        /// Communicates with the UserAccountDAL to determine if userID already exists
        /// </summary>
        /// <param name="userID">User Id to check</param>
        /// <returns>True if the user ID is unquie</returns>
        public bool IsUniqueUserID(string userID)
        {
            return UserAccountDAL.isUniqueUserID(userID);
        }
    }
}
