﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.Model
{
    /// <summary>
    /// Model class for a test object
    /// </summary>
    public class Test
    {
        public Test()
        {

        }

        /// <summary>
        /// Returns a Duplicate test object
        /// </summary>
        /// <param name="test">test object to duplicate</param>
        public Test(Test test)
        {
            ID = test.ID;
            Name = test.Name;
            Active = test.Active;
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Active { get; set; }
    }
}
