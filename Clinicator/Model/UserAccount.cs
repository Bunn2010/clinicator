﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.Model
{
    /// <summary>
    /// Model class for UserAccount object
    /// </summary>
    public class UserAccount
    {
        public string ID  { get; set; }

        public string Password { get; set; }

        public int EmployeeID  { get; set; }

        public string Salt { get; set; }

        /// <summary>
        /// Generates a hashed password
        /// </summary>
        /// <param name="clearTextPassword"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static string generateHashPassword(string clearTextPassword, string salt)
        {
            string saltedPassword = clearTextPassword + salt;
            byte[] saltedPasswordBytes = Encoding.UTF8.GetBytes(saltedPassword);
            System.Security.Cryptography.HashAlgorithm algorithm = new System.Security.Cryptography.SHA256Managed();
            byte[] hash = algorithm.ComputeHash(saltedPasswordBytes);
            return Convert.ToBase64String(hash);
        }
    }
}
