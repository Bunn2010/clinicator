﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.Model
{
    /// <summary>
    /// Model class for a state object
    /// </summary>
    public class State
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
