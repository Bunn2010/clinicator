﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.Model
{

    /// <summary>
    /// Model class for Employee object
    /// </summary>
    public class Employee
    {

        public Employee() { }

        /// <summary>
        /// Makes a duplicate object of the given employee
        /// </summary>
        /// <param name="employee">given employee</param>
        public Employee(Employee employee)
        {
            ID = employee.ID;
            SSN = employee.SSN;
            FirstName = employee.FirstName;
            MiddleInitial = employee.MiddleInitial;
            LastName = employee.LastName;
            DOB = employee.DOB;
            Gender = employee.Gender;
            Address1 = employee.Address1;
            Address2 = employee.Address2;
            City = employee.City;
            StateCode = employee.StateCode;
            Zip = employee.Zip;
            Phone = employee.Phone;
            Role = employee.Role;
            Active = employee.Active;
            UserAccount = employee.UserAccount;
        }

        public int ID { get; set; }

        public int SSN { get; set; }

        public string  FirstName { get; set; }

        public string  MiddleInitial { get; set; }

        public string  LastName { get; set; }

        public DateTime DOB  { get; set; }

        public string Gender  { get; set; }

        public string Address1  { get; set; }

        public string Address2  { get; set; }

        public string City  { get; set; }

        public string StateCode  { get; set; }

        public int Zip  { get; set; }

        public string Phone  { get; set; }

        public string Role  { get; set; }

        public string Active { get; set; }

        public UserAccount UserAccount { get; set; }

        /// <summary>
        /// Returns canotated Full name of Employee
        /// </summary>
        public string FullName
        {
            get
            {
                if (String.IsNullOrEmpty(MiddleInitial))
                    return FirstName + " " + LastName;
                else
                    return FirstName + " " + MiddleInitial + ". " + LastName;
            }
        }

    }

}
