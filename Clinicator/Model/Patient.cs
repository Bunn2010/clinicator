﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.Model
{
    /// <summary>
    /// Model class for Patient object
    /// </summary>
    public class Patient
    {

        public Patient() { }

        /// <summary>
        /// Makes a duplicate instance of a patient object
        /// </summary>
        /// <param name="patient"></param>
        public Patient(Patient patient)
        {
            ID = patient.ID;
            SSN = patient.SSN;
            FirstName = patient.FirstName;
            MiddleInitial = patient.MiddleInitial;
            LastName = patient.LastName;
            DOB = patient.DOB;
            Gender = patient.Gender;
            Address1 = patient.Address1;
            Address2 = patient.Address2;
            City = patient.City;
            StateCode = patient.StateCode;
            Zip = patient.Zip;
            Phone = patient.Phone;
        }

        public int ID { get; set; }

        public int SSN { get; set; }

        public string FirstName { get; set; }

        public string MiddleInitial { get; set; }

        public string LastName { get; set; }

        public DateTime? DOB { get; set; }

        public string Gender { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string StateCode { get; set; }

        public int Zip { get; set; }

        public string Phone { get; set; }

        /// <summary>
        /// Returns Fullname of the Patient
        /// </summary>
        public string PatientName
        {
            get
            {
                if (String.IsNullOrEmpty(MiddleInitial))
                    return FirstName + " " + LastName;
                else
                    return FirstName + " " + MiddleInitial + ". " + LastName;
            }
        }

    }
}
