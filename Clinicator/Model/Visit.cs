﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.Model
{
    /// <summary>
    /// Model class for a Visit Object
    /// </summary>
    public class Visit
    {


        public Visit() { }

        /// <summary>
        /// Makes a duplicate instance of a patient object
        /// </summary>
        /// <param name="patient"></param>
        public Visit(Visit visit)
        {
            ID = visit.ID;
            PatientID = visit.PatientID;
            NurseID = visit.NurseID;
            DoctorID = visit.DoctorID;
            Datetime = visit.Datetime;
            InitialDiagnosis = visit.InitialDiagnosis;
            FinalDiagnosis = visit.FinalDiagnosis;
            FinalDiagnosisDate = visit.FinalDiagnosisDate;
            Height = visit.Height;
            Weight = visit.Weight;
            BloodPressureSystolic = visit.BloodPressureSystolic;
            BloodPressureDiastolic = visit.BloodPressureDiastolic;
            Pulse = visit.Pulse;
            Temperature = visit.Temperature;
            Symptoms = visit.Symptoms;
        }


        // Auto Implemented properties
        public int ID { get; set; }
        public int PatientID { get; set; }
        public int NurseID { get; set; }
        public int DoctorID { get; set; }
        public DateTime Datetime { get; set; }
        public string InitialDiagnosis { get; set; }
        public string FinalDiagnosis { get; set; }
        public DateTime? FinalDiagnosisDate { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public int BloodPressureSystolic { get; set; }
        public int BloodPressureDiastolic { get; set; }
        public string Pulse { get; set; }
        public decimal Temperature { get; set; }
        public string Symptoms { get; set; }
        
    }
}
