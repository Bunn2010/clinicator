﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.DBAccess
{
    /// <summary>
    /// Connection to the CS6232_G3 Database
    /// </summary>
    class ClinicatorDBConnection
    {

        /// <summary>
        /// Connect to the database
        /// </summary>
        /// <returns>Database SQL Connection</returns>
        public static SqlConnection GetConnection()
        {
            string connectionString =
                "Data Source=localhost;Initial Catalog=CS6232_G3;" +
                "Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);
            return connection;
        }

    }
}
