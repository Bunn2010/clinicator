﻿using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.DBAccess
{
    /// <summary>
    /// DAL class to deal with TestOrders
    /// </summary>
    public class TestOrderDAL
    {
        /// <summary>
        /// Creates a new TestOrder that is associated with the given Visit
        /// </summary>
        /// <param name="visitID">ID of the Visit the TestOrder is to be associated with</param>
        /// <param name="testID">ID of the test to add to the TestOrder</param>
        /// <returns></returns>
        public static bool OrderTest(int visitID, int testID) 
        {
             string insertStatement =
                "INSERT INTO TestOrder " +
                  "(visitID, testID) " +
                "VALUES (@VisitID, @TestID)";
            try
            {

                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@VisitID", visitID);
                        insertCommand.Parameters.AddWithValue("@TestID", testID);

                        connection.Open();
                        int count = insertCommand.ExecuteNonQuery();
                        if (count > 0)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// Retrieves list of TestOrders that correspond with the given VisitID
        /// </summary>
        /// <param name="visitID">ID of the given Visit</param>
        /// <returns>List of test orders for the given visit</returns>
        public static List<TestOrder> GetTestOrderListByVisitID(int visitID)
        {
            List<TestOrder> testOrderList = new List<TestOrder>();

            String selectStatement =
                "SELECT * FROM testOrder " +
                " JOIN test " +
                "   ON testID = id " +
                "WHERE visitID = @VisitID " +
                "ORDER BY results, name";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@VisitID", visitID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TestOrder testOrder = new TestOrder();
                                testOrder.VisitID = (int)reader["visitID"];
                                testOrder.TestID = (int)reader["testID"];
                                testOrder.TestName = reader["name"].ToString();
                                if (reader["datePerformed"] != System.DBNull.Value)
                                    testOrder.DatePerformed = Convert.ToDateTime(reader["datePerformed"]);
                                else
                                    testOrder.DatePerformed = null;
                                testOrder.Results = reader["results"].ToString();
                                testOrderList.Add(testOrder);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return testOrderList;
        }

        /// <summary>
        /// Updates the TestOrder containing the corresponding VisitID and TestID with the 
        /// DatePerformed and Result
        /// </summary>
        /// <param name="testOrder">TestOrder object containing the updated data</param>
        /// <returns>True if successful and False if not</returns>
        public static bool UpdateTestOrder(TestOrder oldTestOrder, TestOrder newTestOrder)
        {
            string updateStatement =
                  "UPDATE TestOrder SET " +
                  "datePerformed = @NewDatePerformed, " +
                  "results = @NewResults " +
                  "WHERE visitID = @OldVisitID " +
                  "AND testID = @OldTestID " +
                  "AND (results = @OldResults " +
                      "OR results IS NULL AND @OldResults IS NULL) " +
                  "AND (datePerformed = @OldDatePerformed " +
                      "OR datePerformed IS NULL AND @OldDatePerformed IS NULL) ";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand updateCommand = new SqlCommand(updateStatement, connection))
                    {
                        updateCommand.Parameters.AddWithValue("@NewDatePerformed", newTestOrder.DatePerformed);
                        updateCommand.Parameters.AddWithValue("@NewResults", newTestOrder.Results);
                        
                        updateCommand.Parameters.AddWithValue("@OldVisitID", oldTestOrder.VisitID);
                        updateCommand.Parameters.AddWithValue("@OldTestID", oldTestOrder.TestID);
                        if (String.IsNullOrEmpty(oldTestOrder.Results))
                            updateCommand.Parameters.AddWithValue("@OldResults", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@OldResults", oldTestOrder.Results);
                        if (oldTestOrder.DatePerformed == null)
                            updateCommand.Parameters.AddWithValue("@OldDatePerformed", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@OldDatePerformed", oldTestOrder.DatePerformed);
                        
                        int count = updateCommand.ExecuteNonQuery();
                        if (count > 0)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the TestOrder 
        /// </summary>
        /// <param name="testOrder">TestOrder object containing the order to delete</param>
        /// <returns>True if successful and False if not</returns>
        public static bool DeleteTestOrder(TestOrder testOrder)
        {
            string deleteStatement =
                  "DELETE TestOrder " +
                  "WHERE visitID = @VisitID " +
                  "AND testID = @TestID " + 
                  "AND results is null";  

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand deleteCommand = new SqlCommand(deleteStatement, connection))
                    {
                        deleteCommand.Parameters.AddWithValue("@Results", testOrder.Results);
                        
                        deleteCommand.Parameters.AddWithValue("@VisitID", testOrder.VisitID);
                        deleteCommand.Parameters.AddWithValue("@TestID", testOrder.TestID);
                        
                        int count = deleteCommand.ExecuteNonQuery();
                        if (count > 0)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
