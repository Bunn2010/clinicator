﻿using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.DBAccess
{
    class TestDAL
    {

        /// <summary>
        /// Retrieves a list of active Tests that aren't already on a visit
        /// </summary>
        /// <returns>List of tests</returns>
        public static List<Test> GetActiveTestListNotOrdered(int visitID)
        {
            string selectStatement = "SELECT test.* " +
                                        "FROM test " +
                                            "LEFT JOIN testOrder " +
                                                "ON id = testid and visitID = @visitID " +
                                        "WHERE active = 'Y' and visitID is null " +
                                        "ORDER BY name";

            List<Test> testList = new List<Test>();
            // Load the results into a visit and then put the visit into a list
            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@VisitID", visitID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int idOrd = reader.GetOrdinal("id");
                            int nameOrd = reader.GetOrdinal("name");
                            int activeOrd = reader.GetOrdinal("active");
                            while (reader.Read())
                            {
                                Test test = new Test();
                                test.ID = reader.GetInt32(idOrd);
                                test.Name = reader.GetString(nameOrd);
                                test.Active = reader.GetString(activeOrd);
                                testList.Add(test);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return testList;
        }

        /// <summary>
        /// Retrieves a list of all tests
        /// </summary>
        /// <returns>List of tests</returns>
        public static List<Test> GetTestList()
        {
            string selectStatement = "SELECT * " +
                                        "FROM Test " +
                                        "ORDER BY name";

            List<Test> testList = new List<Test>();
            // Load the results into a visit and then put the visit into a list
            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int idOrd = reader.GetOrdinal("id");
                            int nameOrd = reader.GetOrdinal("name");
                            int activeOrd = reader.GetOrdinal("active");
                            while (reader.Read())
                            {
                                Test test = new Test();
                                test.ID = reader.GetInt32(idOrd);
                                test.Name = reader.GetString(nameOrd);
                                test.Active = reader.GetString(activeOrd);
                                testList.Add(test);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return testList;
        }

        /// <summary>
        /// Creates a new test in the database
        /// </summary>
        /// <param name="test">Test to create</param>
        /// <returns>id of the new test or 0 if no test was created</returns>
        public static int CreateTest(Test test)
        {
            string insertStatement =
                "INSERT INTO Test " +
                "(name, active) " +
                "VALUES (@Name, 'Y')";
            try
            {

                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    
                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        connection.Open();
                        insertCommand.Parameters.AddWithValue("@Name", test.Name.Trim());
                        insertCommand.ExecuteNonQuery();
                        string selectStatement =
                         "SELECT IDENT_CURRENT('Test') from Test";
                        SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                        return Convert.ToInt32(selectCommand.ExecuteScalar());
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updates a Test in the database
        /// </summary>
        /// <param name="oldTest">Original test data</param>
        /// <param name="newTest">Updated test data</param>
        /// <returns></returns>
        public static bool updateTest(Test oldTest, Test newTest)
        {
            string updateStatement =
                "UPDATE test SET " +
                "name = @NewName, " +
                "active = @NewActive " +
                
                "WHERE name = @OldName " +
                "AND active = @OldActive";
            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand updateCommand = new SqlCommand(updateStatement, connection))
                    {
                        
                        updateCommand.Parameters.AddWithValue("@NewName", newTest.Name);
                        updateCommand.Parameters.AddWithValue("@NewActive", newTest.Active);

                        updateCommand.Parameters.AddWithValue("@OldName", oldTest.Name);
                        updateCommand.Parameters.AddWithValue("@OldActive", oldTest.Active);
                       
                        int count = updateCommand.ExecuteNonQuery();
                        if (count > 0)
                            return true;
                        else
                            return false;
                    }
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines if the test name is already present in the DB
        /// </summary>
        /// <param name="name"></param>
        /// <returns>True if there is already a test with the same name</returns>
        public static bool isTestNameDuplicate(string name)
        {
            string selectStatement = "SELECT count(*) " +
                                        "FROM Test " +
                                        "WHERE name = @name";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@name", name.Trim());
                        connection.Open();
                        int count = (int) selectCommand.ExecuteScalar();
                        if (count > 0)
                            return true;
                        else
                            return false;

                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Retrieves a list of tests that match the search criteria
        /// </summary>
        /// <param name="searchTest"></param>
        /// <param name="shouldContainName">True if entire name should be searched</param>
        /// <returns>List of matching tests</returns>
        public static List<Test> searchByTestName(Test searchTest, bool shouldContainName)
        {
            List<Test> testList = new List<Test>();
            String selectStatement = "SELECT id, name, active  " +
                                     "FROM Test " +
                                     "WHERE name LIKE @Name " +
                                      "AND (active = @Active OR @Active IS NULL) " +
                                     "ORDER BY name";
                                                            
             try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();
             using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        if (shouldContainName) 
                            selectCommand.Parameters.AddWithValue("@Name", '%' + searchTest.Name + '%');
                        else
                            selectCommand.Parameters.AddWithValue("@Name", searchTest.Name + '%');
                      
                        if (String.IsNullOrEmpty(searchTest.Active))
                            selectCommand.Parameters.AddWithValue("@Active", DBNull.Value);
                        else
                            selectCommand.Parameters.AddWithValue("@Active", searchTest.Active);

                   using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Test test = new Test();
                                test.ID = (int)reader["id"];
                                test.Name = reader["name"].ToString();
                                test.Active = reader["active"].ToString();
                                testList.Add(test);
                            }
                        }
                    }
                }
            }
             catch (SqlException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
            return testList;

        }
    }
}
