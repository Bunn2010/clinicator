﻿using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Clinicator.DBAccess
{
    /// <summary>
    /// DAL class to deal with Employees
    /// </summary>
    public static class EmployeeDAL
    {

        /// <summary>
        /// Gets a list of the active Employees that have the given role
        /// </summary>
        /// <param name="role">role of the employees</param>
        /// <returns>Returns a list of employees who's role equals given role</returns>
        public static List<Employee> GetActiveEmployeeListByRole(String role)
        {
            List<Employee> employeeList = new List<Employee>();

            String selectStatement =
                "SELECT * FROM employee " +
                "WHERE role = @Role " +
                " AND active = 'Y'";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@Role", role);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Employee employee = new Employee();
                                employee.ID = (int)reader["id"];
                                employee.SSN = (int)reader["ssn"];
                                employee.FirstName = reader["firstName"].ToString();
                                employee.MiddleInitial = reader["middleInitial"].ToString();
                                employee.LastName = reader["lastName"].ToString();
                                employee.DOB = (DateTime)reader["dob"];
                                employee.Gender = reader["gender"].ToString();
                                employee.Address1 = reader["address1"].ToString();
                                employee.Address2 = reader["address2"].ToString();
                                employee.City = reader["city"].ToString();
                                employee.StateCode = reader["stateCode"].ToString();
                                employee.Zip = (int)reader["zip"];
                                employee.Phone = reader["phone"].ToString();
                                employee.Role = reader["role"].ToString();
                                employee.Active = reader["active"].ToString();
                                employeeList.Add(employee);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return employeeList;
        }

        /// <summary>
        /// Gets a list of the Employees that have the given employeeID
        /// </summary>
        /// <param name="employeeID">id of the employees</param>
        /// <returns>Returns a list of employees who's id equals given employeeID</returns>
        public static Employee GetEmployeeByID(int employeeID)
        {
            Employee employee = new Employee();

            String selectStatement =
                "SELECT * FROM employee " +
                "WHERE id = @EmployeeID";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@EmployeeID", employeeID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            while (reader.Read())
                            {
                                employee.ID = (int)reader["id"];
                                employee.SSN = (int)reader["ssn"];
                                employee.FirstName = reader["firstName"].ToString();
                                employee.MiddleInitial = reader["middleInitial"].ToString();
                                employee.LastName = reader["lastName"].ToString();
                                employee.DOB = (DateTime)reader["dob"];
                                employee.Gender = reader["gender"].ToString();
                                employee.Address1 = reader["address1"].ToString();
                                employee.Address2 = reader["address2"].ToString();
                                employee.City = reader["city"].ToString();
                                employee.StateCode = reader["stateCode"].ToString();
                                employee.Zip = (int)reader["zip"];
                                employee.Phone = reader["phone"].ToString();
                                employee.Role = reader["role"].ToString();
                                employee.Active = reader["active"].ToString();
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return employee;
        }

        /// <summary>
        /// Creates a new Employee 
        /// </summary>
        /// <param name="employee">Employee to be created in DB</param>
        /// <returns>ID of new employee</returns>
        public static int CreateEmployee(Employee employee)
        {
            string insertStatement =
                "INSERT Employee " +
                "(ssn, firstName, middleInitial, lastName, dob, gender, address1, address2, city, " +
                "stateCode, zip, phone, role, active) " +
                "VALUES (@SSN, @FirstName, @MiddleInitial, @LastName, @DOB, @Gender, @Address1, " +
                "@Address2, @City, @StateCode, @Zip, @Phone, @Role, @Active)";
            try
            {

                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        connection.Open();
                        using (SqlTransaction employeeTran = connection.BeginTransaction())
                        {

                            insertCommand.Transaction = employeeTran;
                            insertCommand.Parameters.Clear();
                            insertCommand.Parameters.AddWithValue("@SSN", employee.SSN);
                            insertCommand.Parameters.AddWithValue("@FirstName", employee.FirstName.Trim());

                            if (String.IsNullOrEmpty(employee.MiddleInitial))
                                insertCommand.Parameters.AddWithValue("@MiddleInitial", System.DBNull.Value);
                            else
                                insertCommand.Parameters.AddWithValue("@MiddleInitial", employee.MiddleInitial);

                            insertCommand.Parameters.AddWithValue("@LastName", employee.LastName.Trim());
                            insertCommand.Parameters.AddWithValue("@DOB", employee.DOB);
                            insertCommand.Parameters.AddWithValue("@Gender", employee.Gender);
                            insertCommand.Parameters.AddWithValue("@Address1", employee.Address1.Trim());

                            if (String.IsNullOrEmpty(employee.Address2))
                                insertCommand.Parameters.AddWithValue("@Address2", System.DBNull.Value);
                            else
                                insertCommand.Parameters.AddWithValue("@Address2", employee.Address2.Trim());

                            insertCommand.Parameters.AddWithValue("@City", employee.City.Trim());
                            insertCommand.Parameters.AddWithValue("@StateCode", employee.StateCode);
                            insertCommand.Parameters.AddWithValue("@Zip", employee.Zip);
                            insertCommand.Parameters.AddWithValue("@Phone", employee.Phone);
                            insertCommand.Parameters.AddWithValue("@Role", employee.Role);
                            insertCommand.Parameters.AddWithValue("@Active", employee.Active);
                            insertCommand.ExecuteNonQuery();

                            string selectStatement =
                               "SELECT IDENT_CURRENT('Employee') from Employee";
                            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                            selectCommand.Transaction = employeeTran;
                            int employeeID = Convert.ToInt32(selectCommand.ExecuteScalar());

                            // If no user account for system access is needed, just return the employee id that was added
                            if (employee.UserAccount == null)
                            {
                                employeeTran.Commit();
                                return employeeID;
                            }

                            // if user account is needed for system access, attempt to add that first before committing the transaction
                            employee.UserAccount.EmployeeID = employeeID;
                            string insertUserAccountStatement =
                                "INSERT INTO userAccount " +
                                    "(id, password, employeeID, salt)" +
                                "VALUES (@ID, @Password, @EmployeeID, @Salt)";
                            SqlCommand insertUserAccountCommand = new SqlCommand(insertUserAccountStatement, connection);
                            insertUserAccountCommand.Transaction = employeeTran;

                            insertUserAccountCommand.Parameters.AddWithValue("@ID", employee.UserAccount.ID.Trim());
                            insertUserAccountCommand.Parameters.AddWithValue("@Password", employee.UserAccount.Password);
                            insertUserAccountCommand.Parameters.AddWithValue("@EmployeeID", employee.UserAccount.EmployeeID);
                            insertUserAccountCommand.Parameters.AddWithValue("@Salt", employee.UserAccount.Salt);

                            int count = insertUserAccountCommand.ExecuteNonQuery();
                            if (count > 0)
                            {
                                employeeTran.Commit();
                                return employeeID;
                            }
                            else
                            {
                                employeeTran.Rollback();
                                return 0;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get employee role types
        /// </summary>
        /// <returns></returns>
        public static List<Employee> GetEmployeeRoles()
        {
            List<Employee> employeeRoleList = new List<Employee>();

            string selectStatement =
                "SELECT DISTINCT role " +
                "FROM Employee " +
                "ORDER BY role";

             try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                             while (reader.Read())
                            {
                                Employee employee = new Employee();
                                employee.Role = reader["role"].ToString();
                                employeeRoleList.Add(employee);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return employeeRoleList;
        
        }

        /// <summary>
        /// Gets a list of employees that match the search attributes in the employee parameter
        /// </summary>
        /// <param name="searchEmployee">Employee parameter</param>
        /// <returns>List of employees that match</returns>
        public static List<Employee> GetEmployeesBySearchFields(Employee searchEmployee)
        {
            List<Employee> employeeList = new List<Employee>();
            String selectStatement;


            if (searchEmployee.Role == null)
            {
                selectStatement =
                            "SELECT * " +
                            "FROM  employee " +
                            "WHERE firstName LIKE @FirstName " +
                            "AND lastName LIKE @LastName " +
                            "ORDER BY role, lastName, firstName";
            }
            else
            {
                selectStatement =
                            "SELECT * " +
                            "FROM  employee " +
                            "WHERE firstName LIKE @FirstName " +
                            "AND lastName LIKE @LastName " +
                            "AND role LIKE @Role " +
                            "ORDER BY role, lastName, firstName";
            }


            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {

                        selectCommand.Parameters.AddWithValue("@FirstName", searchEmployee.FirstName + '%');
                        selectCommand.Parameters.AddWithValue("@LastName", searchEmployee.LastName + '%');
                        if (searchEmployee.Role != null)
                            selectCommand.Parameters.AddWithValue("@Role", searchEmployee.Role + '%');

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Employee employee = new Employee();
                                employee.ID = (int)reader["id"];
                                employee.SSN = (int)reader["ssn"];
                                employee.FirstName = reader["firstName"].ToString();
                                employee.MiddleInitial = reader["middleInitial"].ToString();
                                employee.LastName = reader["lastName"].ToString();
                                employee.DOB = (DateTime)reader["dob"];
                                employee.Gender = reader["gender"].ToString();
                                employee.Address1 = reader["address1"].ToString();
                                employee.Address2 = reader["address2"].ToString();
                                employee.City = reader["city"].ToString();
                                employee.StateCode = reader["stateCode"].ToString();
                                employee.Zip = (int)reader["zip"];
                                employee.Phone = reader["phone"].ToString();
                                employee.Role = reader["role"].ToString();
                                employee.Active = reader["active"].ToString();
                                employeeList.Add(employee);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return employeeList;
        }

        /// <summary>
        /// Updates an Employee
        /// </summary>
        /// <param name="oldEmployee">Old employee information</param>
        /// <param name="newEmployee">New employee information</param>
        /// <returns>True if updated, false otherwise</returns>
        public static bool UpdateEmployee(Employee oldEmployee, Employee newEmployee)
        {
            string updateStatement =
                   "UPDATE Employee SET " +
                   "ssn = @NewSSN, " +
                   "firstName = @NewFirstName, " +
                   "middleInitial = @NewMiddleInitial, " +
                   "lastName = @NewLastName, " +
                   "dob = @NewDOB, " +
                   "gender = @NewGender, " +
                   "address1 = @NewAddress1, " +
                   "address2 = @NewAddress2, " +
                   "city = @NewCity, " +
                   "stateCode = @NewStateCode, " +
                   "zip = @NewZip, " +
                   "phone = @NewPhone, " +
                   "role = @NewRole, " +
                   "active = @NewActive " +
                   "WHERE id = @OldID " +
                   "AND ssn = @OldSSN " +
                   "AND firstName = @OldFirstName " +
                   "AND (middleInitial = @OldMiddleInitial " +
                        "OR middleInitial IS NULL AND @OldMiddleInitial IS NULL) " +
                   "AND lastName = @OldLastName " +
                   "AND dob = @OldDOB " +
                   "AND gender = @OldGender " +
                   "AND address1 = @OldAddress1 " +
                   "AND (address2 = @OldAddress2 " +
                       "OR address2 IS NULL AND @OldAddress2 IS NULL) " +
                   "AND city = @OldCity " +
                   "AND stateCode = @OldStateCode " +
                   "AND zip = @OldZip " +
                   "AND phone = @OldPhone " +
                   "AND role = @OldRole " +
                   "AND active = @OldActive";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand updateCommand = new SqlCommand(updateStatement, connection))
                    {
                        updateCommand.Parameters.AddWithValue("@NewSSN", newEmployee.SSN);
                        updateCommand.Parameters.AddWithValue("@NewFirstName", newEmployee.FirstName);
                        if (String.IsNullOrEmpty(newEmployee.MiddleInitial))
                            updateCommand.Parameters.AddWithValue("@NewMiddleInitial", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@NewMiddleInitial", newEmployee.MiddleInitial);
                        updateCommand.Parameters.AddWithValue("@NewLastName", newEmployee.LastName);
                        updateCommand.Parameters.AddWithValue("@NewDOB", newEmployee.DOB);
                        updateCommand.Parameters.AddWithValue("@NewGender", newEmployee.Gender);
                        updateCommand.Parameters.AddWithValue("@NewAddress1", newEmployee.Address1);
                        if (String.IsNullOrEmpty(newEmployee.Address2))
                            updateCommand.Parameters.AddWithValue("@NewAddress2", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@NewAddress2", newEmployee.Address2);
                        updateCommand.Parameters.AddWithValue("@NewCity", newEmployee.City);
                        updateCommand.Parameters.AddWithValue("@NewStateCode", newEmployee.StateCode);
                        updateCommand.Parameters.AddWithValue("@NewZip", newEmployee.Zip);
                        updateCommand.Parameters.AddWithValue("@NewPhone", newEmployee.Phone);
                        updateCommand.Parameters.AddWithValue("@NewRole", newEmployee.Role);
                        updateCommand.Parameters.AddWithValue("@NewActive", newEmployee.Active);

                        updateCommand.Parameters.AddWithValue("@OldID", oldEmployee.ID);
                        updateCommand.Parameters.AddWithValue("@OldSSN", oldEmployee.SSN);
                        updateCommand.Parameters.AddWithValue("@OldFirstName", oldEmployee.FirstName);
                        if (String.IsNullOrEmpty(oldEmployee.MiddleInitial))
                            updateCommand.Parameters.AddWithValue("@OldMiddleInitial", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@OldMiddleInitial", oldEmployee.MiddleInitial);
                        updateCommand.Parameters.AddWithValue("@OldLastName", oldEmployee.LastName);
                        updateCommand.Parameters.AddWithValue("@OldDOB", oldEmployee.DOB);
                        updateCommand.Parameters.AddWithValue("@OldGender", oldEmployee.Gender);
                        updateCommand.Parameters.AddWithValue("@OldAddress1", oldEmployee.Address1);
                        if (String.IsNullOrEmpty(oldEmployee.Address2))
                            updateCommand.Parameters.AddWithValue("@OldAddress2", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@OldAddress2", oldEmployee.Address2);
                        updateCommand.Parameters.AddWithValue("@OldCity", oldEmployee.City);
                        updateCommand.Parameters.AddWithValue("@OldStateCode", oldEmployee.StateCode);
                        updateCommand.Parameters.AddWithValue("@OldZip", oldEmployee.Zip);
                        updateCommand.Parameters.AddWithValue("@OldPhone", oldEmployee.Phone);
                        updateCommand.Parameters.AddWithValue("@OldRole", oldEmployee.Role);
                        updateCommand.Parameters.AddWithValue("@OldActive", oldEmployee.Active);

                        int count = updateCommand.ExecuteNonQuery();
                        if (count > 0)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Checks to see if employee ssn already exists in DB
        /// </summary>
        /// <param name="ssn">employee ssn to be checked for uniqueness</param>
        /// <returns>True if ssn is unique False if it already exists in DB</returns>
        public static bool IsEmployeeSSNUnique(int ssn)
        {
            List<int> SSNList = new List<int>();

            String selectStatement =
                "SELECT * FROM employee";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SSNList.Add((int)reader["ssn"]);
                            }
                        }
                    }
                }
                for (int i = 0; i < SSNList.Count(); i++)
                {
                    if (SSNList[i] == ssn)
                        return false;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}
