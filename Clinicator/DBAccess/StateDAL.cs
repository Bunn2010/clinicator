﻿using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.DBAccess
{
    
    /// <summary>
    /// DAL class to deal with States
    /// </summary>
    class StateDAL
    {

        /// <summary>
        /// Retrieves a list of states
        /// </summary>
        /// <returns>List of states</returns>
        public static List<State> GetStateList()
        {
            List<State> stateList = new List<State>();

            string selectStatement =
                "SELECT code, name " +
                "FROM State " +
                "ORDER BY name";

            // Load the restuls into a visit and then put the visit into a list
            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int codeOrd = reader.GetOrdinal("code");
                            int nameOrd = reader.GetOrdinal("name");
                            while (reader.Read())
                            {
                                State state = new State();
                                state.Code = reader.GetString(codeOrd);
                                state.Name = reader.GetString(nameOrd);
                                stateList.Add(state);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return stateList;
        }

    }
}
