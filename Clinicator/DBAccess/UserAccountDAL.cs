﻿using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Clinicator.DBAccess
{
    /// <summary>
    /// DAL class to deal with UserAccounts
    /// </summary>
    public static class UserAccountDAL
    {
        /// <summary>
        /// Gets UserAccount associated with the given employeeID
        /// </summary>
        /// <param name="employeeID">ID of employee UserAccount in associated with</param>
        /// <returns>Returns UserAccount with the given employeeID</returns>
        public static UserAccount GetUserAccountByEmployeeID(int employeeID)
        {
            UserAccount userAccount = new UserAccount();

            String selectStatement =
                "SELECT * FROM userAccount " +
                "WHERE EmployeeID = @EmployeeID";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@EmployeeID", employeeID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            while (reader.Read())
                            {
                                userAccount.ID = reader["id"].ToString();
                                userAccount.Password = reader["passWord"].ToString();
                                userAccount.EmployeeID = (int)reader["employeeID"];
                                userAccount.Salt = reader["salt"].ToString().Trim();
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return userAccount;
        }

        /// <summary>
        /// Gets UserAccount that contains given employeeID and password
        /// </summary>
        /// <param name="userAccountID">id of Employee</param>
        /// <param name="password">password of UserAccount</param>
        /// <returns>UserAccount that contains given employeeID and password</returns>
        public static UserAccount GetUserAccountByUserAccountIDAndPassword(String userAccountID, String clearTextPassword)
        {
            UserAccount userAccount = new UserAccount();
            String password;
            String salt = GetUserAccountSalt(userAccountID);

            if (String.IsNullOrEmpty(salt))
                password = clearTextPassword;
            else
                password = UserAccount.generateHashPassword(clearTextPassword, salt.Trim());
                

            String selectStatement =
                "SELECT ua.* FROM userAccount ua " +
                "JOIN employee e " +
                    "ON ua.employeeID = e.ID and e.active = 'Y' " +
                "WHERE ua.id COLLATE Latin1_General_CS_AS = @UserAccountID AND " +
                "password = @Password";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@UserAccountID", userAccountID);
                        selectCommand.Parameters.AddWithValue("@Password", password);
                        using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            while (reader.Read())
                            {
                                userAccount.ID = reader["id"].ToString();
                                userAccount.Password = reader["password"].ToString();
                                userAccount.EmployeeID = (int)reader["employeeID"];
                                userAccount.Salt = reader["salt"].ToString().Trim();
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return userAccount;
        }


        /// <summary>
        /// Retrieve the user salt used for hashing
        /// </summary>
        /// <param name="userAccountID"></param>
        /// <returns></returns>
        public static string GetUserAccountSalt(String userAccountID)
        {
            String selectStatement =
                "SELECT salt FROM userAccount " +
                "WHERE id = @UserAccountID";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@UserAccountID", userAccountID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            while (reader.Read())
                            {
                                return reader["salt"].ToString().Trim();
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        /// <summary>
        /// Creates a new UserAccount row from the given UserAccount
        /// </summary>
        /// <param name="userAccount">UserAccount to be created in DB</param>
        /// <returns>id of UserAccount to be used as UserName</returns>
        public static bool CreateUserAccount(UserAccount userAccount)
        {

            string insertStatement =
                "INSERT INTO userAccount " +
                "(id, password, employeeID, salt)" +
                "VALUES (@ID, @Password, @EmployeeID, @Salt)";
            try
            {

                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {

                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@ID", userAccount.ID.Trim());
                        insertCommand.Parameters.AddWithValue("@Password", userAccount.Password);
                        insertCommand.Parameters.AddWithValue("@EmployeeID", userAccount.EmployeeID);
                        insertCommand.Parameters.AddWithValue("@Salt", userAccount.Salt);
                       
                        connection.Open();
                        int count = insertCommand.ExecuteNonQuery();
                        if (count > 0)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Updates the password on a user account
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="newUserAccount"></param>
        /// <returns>True if update was successful</returns>
        public static bool UpdatePassword(UserAccount userAccount, UserAccount newUserAccount)
        {
            string updateStatement;


            updateStatement =
                 "UPDATE UserAccount " +
                   "SET password = @NewPassword " +
                 "WHERE ID = @ID and password = @Password"; 


            try
            {

                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    using (SqlCommand updateCommand = new SqlCommand(updateStatement, connection)) 
                    {
                        updateCommand.Parameters.AddWithValue("@ID", userAccount.ID);
                        updateCommand.Parameters.AddWithValue("@Password", userAccount.Password);
                        updateCommand.Parameters.AddWithValue("@NewPassword", newUserAccount.Password);

                        connection.Open();
                        int count = updateCommand.ExecuteNonQuery();
                        if (count > 0)
                            return true;
                        else
                            return false;
                        }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete user account
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="newUserAccount"></param>
        /// <returns>True if delete was successful</returns>
        public static bool DeleteAccount(UserAccount userAccount)
        {
            string deleteStatement = "DELETE FROM UserAccount " +
                                          "WHERE ID = @ID";

            try
            {

                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    using (SqlCommand deleteCommand = new SqlCommand(deleteStatement, connection))
                    {
                        deleteCommand.Parameters.AddWithValue("@ID", userAccount.ID);

                        connection.Open();
                        int count = deleteCommand.ExecuteNonQuery();
                        if (count > 0)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines if the userID is already present in the DB
        /// </summary>
        /// <param name="name"></param>
        /// <returns>True if there is already a userID with the same name</returns>
        public static bool isUniqueUserID(string userID)
        {
            string selectStatement = "SELECT count(*) " +
                                        "FROM UserAccount " +
                                        "WHERE id = @UserID";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@UserID", userID.Trim());
                        connection.Open();
                        int count = (int)selectCommand.ExecuteScalar();
                        if (count > 0)
                            return false;
                        else
                            return true;

                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }
    
}
