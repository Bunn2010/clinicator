﻿using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.DBAccess
{
    /// <summary>
    /// DAL class to deal with Visits
    /// </summary>
    class VisitDAL
    {

        /// <summary>
        /// Retrieves a list of visit dates for the specified patient
        /// </summary>
        /// <param name="patientID">ID of the patient</param>
        /// <returns>List of visits for the patient</returns>
        public static List<Visit> GetVisitDateList(int patientID)
        {
            List<Visit> visitList = new List<Visit>();

            string selectStatement =
                "SELECT id, datetime " +
                "FROM Visit " +
                "WHERE patientID = @patientID " +
                "ORDER BY datetime desc";

            // Load the restuls into a visit and then put the visit into a list
            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@patientID", patientID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int visitIDOrd = reader.GetOrdinal("id");
                            int datetimeOrd = reader.GetOrdinal("datetime");
                            while (reader.Read())
                            {
                                Visit visit = new Visit();
                                visit.ID = reader.GetInt32(visitIDOrd);
                                visit.Datetime = reader.GetDateTime(datetimeOrd);
                                visitList.Add(visit);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return visitList;
        }

        /// <summary>
        /// Retreives a visit
        /// </summary>
        /// <param name="visitID">ID of the visit to return</param>
        /// <returns>A visit object or null if no matching visit</returns>
        public static Visit GetVisit(int visitID)
        {
            Visit visit = new Visit();
            string selectStatement =
                "SELECT *  " +
                "FROM visit " +
                "WHERE id = @visitID";
            try
            {

                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@visitID", visitID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            int visitIDOrd = reader.GetOrdinal("id");
                            int patientIDOrd = reader.GetOrdinal("patientID");
                            int nurseIDOrd = reader.GetOrdinal("nurseID");
                            int doctorIDOrd = reader.GetOrdinal("doctorID");
                            int datetimeOrd = reader.GetOrdinal("datetime");
                            int initialDiagnosisOrd = reader.GetOrdinal("initialDiagnosis");
                            int finalDiagnosisOrd = reader.GetOrdinal("finalDiagnosis");
                            int finalDiagnosisDateOrd = reader.GetOrdinal("finalDiagnosisDate");
                            int heightOrd = reader.GetOrdinal("height");
                            int weightOrd = reader.GetOrdinal("weight");
                            int bloodPressureSystolicOrd = reader.GetOrdinal("bloodPressureSystolic");
                            int bloodPressureDiastolicOrd = reader.GetOrdinal("bloodPressureDiastolic");
                            int pulseOrd = reader.GetOrdinal("pulse");
                            int temperatureOrd = reader.GetOrdinal("temperature");
                            int symptomsOrd = reader.GetOrdinal("symptoms");
                            if (reader.Read())
                            {
                                visit.ID = reader.GetInt32(visitIDOrd);
                                visit.PatientID = reader.GetInt32(patientIDOrd);
                                visit.NurseID = reader.GetInt32(nurseIDOrd);
                                visit.DoctorID = reader.GetInt32(doctorIDOrd);
                                visit.Datetime = reader.GetDateTime(datetimeOrd);

                                if (!reader.IsDBNull(initialDiagnosisOrd))
                                    visit.InitialDiagnosis = reader.GetString(initialDiagnosisOrd);

                                if (!reader.IsDBNull(finalDiagnosisOrd))
                                    visit.FinalDiagnosis = reader.GetString(finalDiagnosisOrd);
                                
                                if (!reader.IsDBNull(finalDiagnosisDateOrd))
                                    visit.FinalDiagnosisDate = reader.GetDateTime(finalDiagnosisDateOrd);
                                else
                                    visit.FinalDiagnosisDate = null;

                                visit.Height = reader.GetDecimal(heightOrd);
                                visit.Weight = reader.GetDecimal(weightOrd);
                                visit.BloodPressureSystolic = reader.GetInt32(bloodPressureSystolicOrd);
                                visit.BloodPressureDiastolic = reader.GetInt32(bloodPressureDiastolicOrd);
                                visit.Pulse = reader.GetString(pulseOrd);
                                visit.Temperature = reader.GetDecimal(temperatureOrd);
                                visit.Symptoms = reader.GetString(symptomsOrd);

                            }
                            else
                            {
                                visit = null;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return visit;
        }


        /// <summary>
        /// Creates a new visit
        /// </summary>
        /// <param name="visit"></param>
        /// <returns>Id of the new visit</returns>
        public static int CreateVisit(Visit visit)
        {

            string insertStatement =
                "INSERT Visit " +
                  "(patientID, nurseID, doctorID, datetime, height, weight, bloodPressureSystolic, bloodPressureDiastolic, pulse, temperature, symptoms) " +
                "VALUES (@patientID, @nurseID, @doctorID, GETDATE(), @height, @weight, @bloodPressureSystolic, @bloodPressureDiastolic, @pulse, @temperature, @symptoms)";
            try
            {

                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@patientID", visit.PatientID);
                        insertCommand.Parameters.AddWithValue("@nurseID", visit.NurseID);
                        insertCommand.Parameters.AddWithValue("@doctorID", visit.DoctorID);
                        insertCommand.Parameters.AddWithValue("@height", visit.Height);
                        insertCommand.Parameters.AddWithValue("@weight", visit.Weight);
                        insertCommand.Parameters.AddWithValue("@bloodPressureSystolic", visit.BloodPressureSystolic);
                        insertCommand.Parameters.AddWithValue("@bloodPressureDiastolic", visit.BloodPressureDiastolic);
                        insertCommand.Parameters.AddWithValue("@pulse", visit.Pulse);
                        insertCommand.Parameters.AddWithValue("@temperature", visit.Temperature);
                        insertCommand.Parameters.AddWithValue("@symptoms", visit.Symptoms);
                        insertCommand.ExecuteNonQuery();
                        string selectStatement =
                            "SELECT IDENT_CURRENT('Visit') from Visit";
                        SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                        return Convert.ToInt32(selectCommand.ExecuteScalar());
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        /// <summary>
        /// Updates the Visit Diagnosis fields only
        /// </summary>
        /// <param name="oldVisit"></param>
        /// <param name="newVisit"></param>
        /// <returns>True if successfully updated</returns>
        public static bool UpdateVisitDiagnosis(Visit oldVisit, Visit newVisit)
        {
            string updateStatement;
            {
                if (String.IsNullOrEmpty(newVisit.FinalDiagnosis))
                    updateStatement =
                        "UPDATE visit " +
                            "SET initialDiagnosis = @NewInitialDiagnosis " + 
                      "WHERE id = @OldID " +
                      "AND initialDiagnosis is null " +
                      "AND finalDiagnosis is null " +
                      "AND finalDiagnosisDate is null";
                else 
                    updateStatement = 
                        "UPDATE visit " +
                            "SET initialDiagnosis = @NewInitialDiagnosis, " + 
                                "finalDiagnosis = @NewFinalDiagnosis, " +
                                "finalDiagnosisDate = GETDATE() " +
                      "WHERE id = @OldID " +
                      "AND initialDiagnosis = @OldInitialDiagnosis " +
                      "AND finalDiagnosis is null " +
                      "AND finalDiagnosisDate is null";

                try
                {
                    using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                    {
                        connection.Open();
                        using (SqlCommand updateCommand = new SqlCommand(updateStatement, connection))
                        {
                            updateCommand.Parameters.AddWithValue("@NewInitialDiagnosis", newVisit.InitialDiagnosis);
                            if (String.IsNullOrEmpty(newVisit.FinalDiagnosis))
                                updateCommand.Parameters.AddWithValue("@NewFinalDiagnosis", DBNull.Value);
                            else
                                updateCommand.Parameters.AddWithValue("@NewFinalDiagnosis", newVisit.FinalDiagnosis);

                            updateCommand.Parameters.AddWithValue("@OldID", oldVisit.ID);
                            if (newVisit.FinalDiagnosis != null)
                                updateCommand.Parameters.AddWithValue("@OldInitialDiagnosis", oldVisit.InitialDiagnosis);
     

                            int count = updateCommand.ExecuteNonQuery();
                            if (count > 0)
                                return true;
                            else
                                return false;
                        }
                    }
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
