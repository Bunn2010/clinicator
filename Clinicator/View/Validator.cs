﻿using Clinicator.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinicator.Model
{
    /// <summary>
    /// Validates text and combo box input on form data
    /// </summary>
    class Validator
    {
        private static string title = "Entry Error";
        public enum LengthType { character, digit };

        /// <summary>
        /// Getter Setter for Title
        /// </summary>
        public static string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        /// <summary>
        /// Checks that data is present in text box or combo box
        /// </summary>
        /// <param name="control"></param>
        /// <returns>True if data present, False otherwise</returns>
        public static bool IsPresent(Control control)
        {
            if (control.GetType().ToString() == "System.Windows.Forms.TextBox")
            {
                TextBox textBox = (TextBox)control;
                if (String.IsNullOrEmpty(textBox.Text.Trim()))
                {
                    MessageBox.Show(textBox.Tag.ToString() + " is a required field.", Title);
                    textBox.Focus();
                    return false;
                }
            }
            else if (control.GetType().ToString() == "System.Windows.Forms.MaskedTextBox")
            {
                MaskedTextBox textBox = (MaskedTextBox)control;
                if (String.IsNullOrEmpty(textBox.Text.Trim()))
                {
                    MessageBox.Show(textBox.Tag.ToString() + " is a required field.", Title);
                    textBox.Focus();
                    return false;
                }
            }
            else if (control.GetType().ToString() == "System.Windows.Forms.ComboBox")
            {
                ComboBox comboBox = (ComboBox)control;
                if (comboBox.SelectedIndex == -1)
                {
                    MessageBox.Show(comboBox.Tag.ToString() + " is a required field.", Title);
                    comboBox.Focus();
                    return false;
                }
            }
            else if (control.GetType().ToString() == "System.Windows.Forms.DateTimePicker")
            {
                DateTimePicker dateTimePicker = (DateTimePicker)control;
                if (!dateTimePicker.Checked)
                {
                    MessageBox.Show(dateTimePicker.Tag.ToString() + " is a required field.", Title);
                    dateTimePicker.Focus();
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Converts text to integer and returns true if converted, false otherwise
        /// </summary>
        /// <param name="textBox">The textbox input</param>
        /// <returns>True or false</returns>
        public static bool IsInt32(Control control)
        {
            string text = "";
            if (control.GetType().ToString() == "System.Windows.Forms.TextBox")
            {
                TextBox textbox = (TextBox)control;
                text = textbox.Text;
            }
            else if (control.GetType().ToString() == "System.Windows.Forms.MaskedTextBox")
            {
                MaskedTextBox textbox = (MaskedTextBox)control;
                text = textbox.Text;
            }
            if (text.Trim().Length > 9)
            {
                MessageBox.Show(control.Tag.ToString() + " must be an integer value less than 999999999", Title);
                control.Focus();
                return false;
            }
            try
            {

                Convert.ToInt32(text);
                return true;
            }
            catch (FormatException)
            {
                MessageBox.Show(control.Tag.ToString() + " must be an integer value.", Title);
                control.Focus();
                return false;
            }
        }

        /// <summary>
        /// Validates if in the valid zip code range for US
        /// </summary>
        /// <param name="textBox">Textbox with zipcode</param>
        /// <param name="firstZip">Lowest zipcode</param>
        /// <param name="lastZip">Highest zipcode</param>
        /// <returns>True if valid, false otherwise</returns>
        public static bool IsStateZipCode(Control control, int firstZip, int lastZip)
        {
            string text = "";
            if (control.GetType().ToString() == "System.Windows.Forms.TextBox")
            {
                TextBox textbox = (TextBox)control;
                text = textbox.Text;
            }
            else if (control.GetType().ToString() == "System.Windows.Forms.MaskedTextBox")
            {
                MaskedTextBox textbox = (MaskedTextBox)control;
                text = textbox.Text;
            }

            int zipCode = Convert.ToInt32(text);
            if (zipCode < firstZip || zipCode > lastZip)
            {
                MessageBox.Show(control.Tag.ToString() + " must be within this range: " +
                    firstZip + " to " + lastZip + ".", Title);
                control.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Validates valid phone number for xxx-xxx-xxxx
        /// </summary>
        /// <param name="textBox">Textbox with phone</param>
        /// <returns>True if valid, false otherwise</returns>
        public static bool IsPhoneNumber(TextBox textBox)
        {
            if (textBox.Text.Trim().Length < 12 || !Regex.IsMatch(textBox.Text.Substring(0,12), @"[0-9]{3}-[0-9]{3}-[0-9]{4}"))
            {
                MessageBox.Show("First 12 positions of " + textBox.Tag.ToString() + " must be in xxx-xxx-xxxx format (where x is 0-9)", Title);
                textBox.Focus();
                return false;
            }
            return true;

        }

        /// <summary>
        /// Compare two text boxes to determine if the values are the same
        /// </summary>
        /// <param name="textbox1"></param>
        /// <param name="textbox2"></param>
        /// <returns>True if the text values match of the two text boxes</returns>
        public static bool IsMatchingText(TextBox textbox1, TextBox textbox2)
        {
            if (textbox1.Text.Trim() != textbox2.Text.Trim())
            {
                MessageBox.Show(textbox1.Tag.ToString() + " must equal " + textbox2.Tag.ToString(), Title);
                textbox1.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Determines if the text box contents without trailing spaces meets the minimum length specified.
        /// </summary>
        /// <param name="textbox"></param>
        /// <param name="minimumLength"></param>
        /// <param name="lengthType"></param>
        /// <returns>True if text box has the minimum length.  False if it does not.</returns>
        public static bool IsMiniumumLenth(TextBox textbox, int minimumLength, LengthType lengthType)
        {
            if (textbox.Text.Trim().Length < minimumLength)
            {
                string lengthTypeText = lengthType.ToString();
                if (minimumLength != 1) lengthTypeText += 's';
                MessageBox.Show(textbox.Tag.ToString() + " must be at least " + minimumLength + " " + lengthTypeText + " long", Title);
                textbox.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Determines if the text box contents without trailing spaces meets the maximum length specified.
        /// </summary>
        /// <param name="textbox"></param>
        /// <param name="maximumLength"></param>
        /// <param name="lengthType"></param>
        /// <returns>True if text box is less than or equal to the maximum length.  False if is does not.</returns>
        public static bool IsMaxiumumLength(TextBox textbox, int maximumLength, LengthType lengthType)
        {
            if (textbox.Text.Trim().Length > maximumLength)
            {
                string lengthTypeText = lengthType.ToString();
                if (maximumLength != 1) lengthTypeText += 's';
                MessageBox.Show(textbox.Tag.ToString() + " must be less than or equal to " + maximumLength + " " + lengthTypeText + " long", Title);
                textbox.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Checks if a text box contains letters only
        /// </summary>
        /// <param name="textbox"></param>
        /// <returns>True if only letters were found.  False if non-letter characters were found</returns>
        public static bool IsLettersAndSpacesOnly(TextBox textbox)
        {
            if (!Regex.IsMatch(textbox.Text, @"^[a-zA-Z ]+$"))
            {
                MessageBox.Show(textbox.Tag.ToString() + " can only contain letters", Title);
                textbox.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        ///Validates valid ssn for xxx-xx-xxxx
        /// </summary>
        /// <param name="textbox">Textbox with ssn</param>
        /// <returns>True if valid, false otherwise</returns>
        public static bool IsSsn(Control control)
        {

            string text = "";
            if (control.GetType().ToString() == "System.Windows.Forms.TextBox")
            {
                TextBox textbox = (TextBox)control;
                text = textbox.Text;
            }
            else if (control.GetType().ToString() == "System.Windows.Forms.MaskedTextBox")
            {
                MaskedTextBox textbox = (MaskedTextBox)control;
                text = textbox.Text;
            }


            if (!Regex.IsMatch(text, @"^\d{3}-?\d{2}-?\d{4}$"))
            {
                MessageBox.Show(control.Tag.ToString() + " must be in xxx-xx-xxxx OR xxxxxxxxx format (where x is 0-9)", Title);
                control.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Checks a text box for a valid decimal number with a maximum number of decimal places
        /// </summary>
        /// <param name="textbox"></param>
        /// <param name="maxDecimalPlaces"></param>
        /// <returns></returns>
        public static bool IsDecimal(TextBox textbox, int maxDecimalPlaces)
        {

            decimal result;
            if (decimal.TryParse(textbox.Text, out result))
            {
                if (textbox.Text.IndexOf(".") == -1 ||
                    textbox.Text.Length - (textbox.Text.IndexOf(".") + 1) <= maxDecimalPlaces)
                    return true;
            }

            MessageBox.Show(textbox.Tag.ToString() + " must be a number with a maximum of " + maxDecimalPlaces + " decimal places", Title);
            textbox.Focus();
            return false;
        }

        /// <summary>
        /// Determines if a text box with a valid decimal value is in the specified range
        /// </summary>
        /// <param name="textbox"></param>
        /// <param name="lowValue"></param>
        /// <param name="highValue"></param>
        /// <returns></returns>
        public static bool IsDecimalInRange(TextBox textbox, int lowValue, int highValue)
        {
            if (Decimal.Parse(textbox.Text) < lowValue || Decimal.Parse(textbox.Text) > highValue)
            {
                MessageBox.Show(textbox.Tag.ToString() + " must be a in the range of " + lowValue + " to " + highValue, Title);
                textbox.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Determines if a text box with a valid integer value is in the specified range
        /// </summary>
        /// <param name="textbox"></param>
        /// <param name="lowValue"></param>
        /// <param name="highValue"></param>
        /// <returns></returns>
        public static bool IsIntegerInRange(TextBox textbox, int lowValue, int highValue)
        {
            if (Convert.ToInt32(textbox.Text) < lowValue || Convert.ToInt32(textbox.Text) > highValue)
            {
                MessageBox.Show(textbox.Tag.ToString() + " must be a in the range of " + lowValue + " to " + highValue, Title);
                textbox.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Deteremines if the number is in the specified range
        /// </summary>
        /// <param name="textbox"></param>
        /// <param name="lowNumber"></param>
        /// <param name="HighNumber"></param>
        /// <returns></returns>
        public static bool IsNumberInRange(TextBox textbox, int lowNumber, int HighNumber)
        {
            string extracted = "";
            string text = textbox.Text;

            for (int i = 0; i < text.Length - 1; i++)
            {
                if (Char.IsDigit(text[i]))
                    extracted += text[i];
            }
            int integer = Convert.ToInt32(extracted);

            if (integer > lowNumber && integer < HighNumber)
            {
                MessageBox.Show(textbox.Tag.ToString() + " must contain a number in the range of " + 
                    lowNumber + " and " + HighNumber, Title);
                textbox.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Determines if a starting date time picker control contains a value which on or before the ending date time picker control
        /// </summary>
        /// <param name="dtpStart"></param>
        /// <param name="dtpEnd"></param>
        /// <returns>True if start date is before end date</returns>
        public static bool IsStartBeforeEndDate(DateTimePicker dtpStart, DateTimePicker dtpEnd )
        {
            if (dtpStart.Value > dtpEnd.Value)
            {
                MessageBox.Show(dtpStart.Tag.ToString() + " must be on or before " + dtpEnd.Tag.ToString(), Title);
                dtpStart.Focus();
                return false;
            }
            return true;
        }


        public static bool IsInDateRange(DateTimePicker dateTimePicker, DateTime minDate, DateTime maxDate)
        {
            if (dateTimePicker.Value < minDate || dateTimePicker.Value > maxDate )
            {
                MessageBox.Show(dateTimePicker.Tag.ToString() + " must be between " + minDate.ToString("d") + " and " + maxDate.ToString("d"), Title);

                dateTimePicker.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Determines if the test name is unique in the database
        /// </summary>
        /// <param name="textbox"></param>
        /// <returns>True if the name is unique</returns>
        public static bool IsTestNameDuplicate(TextBox textbox)
        {
            AdminController adminController = new AdminController();
            if (adminController.isTestNameDuplicate(textbox.Text.Trim()))
            {
                MessageBox.Show(textbox.Text.Trim() + " already exists.  Please Enter another name.");
                textbox.Focus();
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool IsSsnUnique(Object person)
        {
            bool isUnique = true;
            if (person.GetType().ToString() == "Clinicator.Model.Employee")
            {
                try
                {
                    AdminController controller = new AdminController();
                    Employee employee = (Employee)person;
                    isUnique = controller.IsEmployeeSSNUnique(employee.SSN);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            else if (person.GetType().ToString() == "Clinicator.Model.Patient")
            {
                try
                {
                    PatientController controller = new PatientController();
                    Patient patient = (Patient)person;
                    isUnique = controller.IsPatientSSNUnique(patient.SSN);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            if (!isUnique)
            {
                MessageBox.Show("There is already an Employee with that SSN", Title);
            }
            return isUnique;
        }

        public static bool IsUnqiueUserID(TextBox textbox)
        {
            try
            {
                AdminController adminController = new AdminController();
                if (!adminController.IsUniqueUserID(textbox.Text.Trim()))
                {
                    MessageBox.Show(textbox.Tag.ToString() + " already exists.  Please use a different id.");
                    textbox.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            return false;
        }
    }
}
