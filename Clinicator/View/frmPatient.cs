﻿using Clinicator.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clinicator.Controller;
using Clinicator.Model;

namespace Clinicator
{
    /// <summary>
    /// Class for the Patient form and Nurse tasks
    /// </summary>
    public partial class frmPatient : Form
    {
        private PatientController patientController;
        private Patient patient;
        private Patient orgPatient;
        private List<State> stateList;
        private StateController stateController;
        private frmPatientSearch patientSearchForm;
        private AdminController adminController;
        private List<Employee> doctorList;

        private TabPage registrationTab;
        private TabPage visitTab;
        private TabPage diagnosisTab;
        private TabPage testsTab;
        private Visit visit;
        private Visit orgVisit;
        private TestOrder testOrder;
        private TestOrder orgTestOrder;
        private List<Test> testList;

        /// <summary>
        /// Initializes the Patient form
        /// </summary>
        public frmPatient()
        {
            InitializeComponent();
            patientController = new PatientController();
            stateController = new StateController();
            adminController = new AdminController();

            registrationTab = tabControlPatient.TabPages[0];
            visitTab = tabControlPatient.TabPages[1];
            diagnosisTab = tabControlPatient.TabPages[2];
            testsTab = tabControlPatient.TabPages[3];

            ClearControls();
            
        }

        /// <summary>
        /// Property that is set in the parent form of the nurse that is currently logged into the system
        /// </summary>
        public Employee NurseLoggedIn { get; set; }

        /// <summary>
        /// Load the Patient form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPatient_Load(object sender, EventArgs e)
        {
            this.LoadComboBoxes();
            dOBDateTimePicker.Format = DateTimePickerFormat.Custom;
            dOBDateTimePicker.CustomFormat = " ";
            btnNewVisit.Enabled = false;
            setEnabledPropertyOnRegistrationControls(false);
            sSNMaskedTextBox.PromptChar = ' ';
        }

        /// <summary>
        /// Opens Search form to search for Patient
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            patientSearchForm = new frmPatientSearch();
            patientSearchForm.MdiParent = this.ParentForm;
            patientSearchForm.Show();
        }

        /// <summary>
        /// Load the State and Gender combo boxes.
        /// </summary>
        private void LoadComboBoxes()
        {
            genderComboBox.Items.Add("M");
            genderComboBox.Items.Add("F");
            genderComboBox.SelectedIndex = -1;

            try
            {
                stateList = stateController.GetStateList();
                stateCodeComboBox.DataSource = stateList;
                stateCodeComboBox.DisplayMember = "code";
                stateCodeComboBox.ValueMember = "code";
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        /// <summary>
        /// Set the enabled property on the registration form controls to true or false
        /// </summary>
        /// <param name="enabledValue">True if the controls should be enabled or false if they should be disabled</param>
        private void setEnabledPropertyOnRegistrationControls(bool enabledValue)
        {
            patientNameTextBox.Enabled = enabledValue;
            visitComboBox.Enabled = enabledValue;
            firstNameTextBox.Enabled = enabledValue;
            middleInitialTextBox.Enabled = enabledValue;
            lastNameTextBox.Enabled = enabledValue;
            genderComboBox.Enabled = enabledValue;
            sSNMaskedTextBox.Enabled = enabledValue;
            dOBDateTimePicker.Enabled = enabledValue;
            address1TextBox.Enabled = enabledValue;
            address2TextBox.Enabled = enabledValue;
            cityTextBox.Enabled = enabledValue;
            stateCodeComboBox.Enabled = enabledValue;
            zipTextBox.Enabled = enabledValue;
            phoneTextBox.Enabled = enabledValue;

            btnRegistrationSave.Enabled = enabledValue;
            btnRegistrationCancel.Enabled = enabledValue;
        }

        /// <summary>
        /// Clears the controls in the form
        /// </summary>
        private void ClearControls()
        {
            patientBindingSource.Clear();
            visitBindingSource.Clear();
            dgvTestOrders.DataSource = null;
            txtNurseName.Text = "";
            visitComboBox.DataSource = null;
            genderComboBox.SelectedIndex = -1;
            dOBDateTimePicker.Format = DateTimePickerFormat.Custom;
            dOBDateTimePicker.Checked = false;
            tabControlPatient.SelectedTab = tabRegistration;
            // registrationTab.Enabled = false;
            visitTab.Enabled = false;
            diagnosisTab.Enabled = false;
            testsTab.Enabled = false;
        }

        /// <summary>
        /// Adds a patient to the Clinic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRegistrationSave_Click(object sender, EventArgs e)
        {
            if (Validator.IsPresent(firstNameTextBox) &&
                Validator.IsPresent(lastNameTextBox) &&
                Validator.IsPresent(genderComboBox) &&
                Validator.IsSsn(sSNMaskedTextBox) && // Validator.IsMiniumumLenth(sSNTextBox, 9, Validator.LengthType.digit) &&
                Validator.IsPresent(dOBDateTimePicker) && Validator.IsInDateRange(dOBDateTimePicker, DateTime.Now.AddYears(-150), DateTime.Now) &&
                Validator.IsPresent(address1TextBox) &&
                Validator.IsPresent(cityTextBox) && Validator.IsLettersAndSpacesOnly(cityTextBox) &&
                Validator.IsPresent(stateCodeComboBox) &&
                Validator.IsInt32(zipTextBox) && Validator.IsMiniumumLenth(zipTextBox, 5, Validator.LengthType.digit) && Validator.IsStateZipCode(zipTextBox, 00501, 99950) &&
                Validator.IsPresent(phoneTextBox) && Validator.IsPhoneNumber(phoneTextBox))
                {

                    patientBindingSource.EndEdit();
                    if (this.patient.ID == 0)
                    {
                        addRegistration();
                    }
                    else
                    {
                        updateRegistration();
                    }                        
                }
           
        }

        /// <summary>
        /// Attempts to add patient registration information to the database
        /// </summary>
        private void addRegistration()
        {
            patient.SSN = Convert.ToInt32(sSNMaskedTextBox.Text.ToString().Trim());
            try
            {
                if (!Validator.IsSsnUnique(patient))
                    return;
                patient.ID = patientController.CreatePatient(patient);
                MessageBox.Show("Patient was successfully registered");
                iDTextBox.Text = patient.ID.ToString();
                this.displayPatient();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Attempts to update patient registration information in the database
        /// </summary>
        private void updateRegistration()
        {
            patient.SSN = Convert.ToInt32(sSNMaskedTextBox.Text.ToString().Trim());
            try
            {
                if (orgPatient.SSN != patient.SSN && !Validator.IsSsnUnique(patient))
                    return;
                
                if (!patientController.UpdatePatient(orgPatient, patient))
                {
                    MessageBox.Show("Another user has updated or " +
                        "deleted that Patient.", "Database Error");
                    iDTextBox.Text = patient.ID.ToString();
                }
                else
                {
                    MessageBox.Show("Patient was updated successfully");

                }
                this.displayPatient();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }    

        /// <summary>
        /// Gets the data for selected patient
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRetrieve_Click(object sender, EventArgs e)
        {
            displayPatient();
        }

        /// <summary>
        /// Clears controls to add new patient
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewPatient_Click(object sender, EventArgs e)
        {
            iDTextBox.Text = "";
            iDTextBox.Enabled = false;
            this.ClearControls();
            patient = new Patient();
            patientBindingSource.Add(patient);
            sSNMaskedTextBox.Text = "";
            zipTextBox.Text = "";
            registrationTab.Enabled = true;
            setEnabledPropertyOnRegistrationControls(true);
            btnRetrieve.Enabled = false;
            btnNewVisit.Enabled = false;
            btnNewPatient.Enabled = false;
            btnSearch.Enabled = false;
        }

        /// <summary>
        /// Reset to forms initial state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRegistrationCancel_Click(object sender, EventArgs e)
        {
            
            // if cancel the add of a new paitient, reset the entire form.
            if (this.patient.ID == 0)
            {
                resetPatientForm();
            }
            // Otherwise redisplay the current data for the patient
            else
            {
                displayPatient();
            }
        }

        private void resetPatientForm()
        {
            iDTextBox.Enabled = true;
            this.ClearControls();
            this.setEnabledPropertyOnRegistrationControls(false);
            btnRetrieve.Enabled = true;
            btnSearch.Enabled = true;
            btnNewPatient.Enabled = true;
            btnNewVisit.Enabled = false;
        }

        private void displayPatient()
        {

            if (Validator.IsInt32(iDTextBox))
            {
                int patientID = Convert.ToInt32(iDTextBox.Text);

                try
                {
                    patient = patientController.getPatientByID(patientID);
                    if (patient.ID == 0)
                    {
                        MessageBox.Show("No Patient Registered with that ID","Patient Not Found");
                        return;
                    }

                    orgPatient = new Patient(patient);
                    dOBDateTimePicker.Format = DateTimePickerFormat.Short;

                    patientBindingSource.Clear();
                    patientBindingSource.Add(patient);


                    this.loadVisits(patientID);

                    registrationTab.Enabled = true;
                    setEnabledPropertyOnRegistrationControls(true);
                    btnRetrieve.Enabled = true;
                    btnNewVisit.Enabled = true;

                    displayVisit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        private void loadVisits(int patientID)
        {
            List<Visit> visitDateList = patientController.GetVisitDateList(patientID);
            visitComboBox.DataSource = visitDateList;
            visitComboBox.DisplayMember = "datetime";
            visitComboBox.ValueMember = "id";
        }

        private void iDTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.displayPatient();
            }
        }

        private void dOBDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            dOBDateTimePicker.Format = DateTimePickerFormat.Short;
        }


        private void handlePatientSearchResults(object sender, EventArgs e)
        {
            if (patientSearchForm != null && patientSearchForm.patient !=null && patientSearchForm.patient.ID > 0)
            {
                iDTextBox.Text = patientSearchForm.patient.ID.ToString();
                patientSearchForm.Dispose();
                this.displayPatient();
            }            
            
        }

        /// <summary>
        /// Sets Format and Parse events for sSNTextBox
        /// </summary>
        private void SsnBindControl()
        {
            //ssnBinding = sSNTextBox.DataBindings["Text"];
            //ssnBinding.Format += Formatter.FormatSsnNumber;
            //ssnBinding.Parse += Formatter.UnformatSsnNumber;
        }


        private void displayVisit()
        {
            visitBindingSource.Clear();
            txtNurseName.Text = "";
            heightTextBox.Focus();
            if (visitComboBox.SelectedIndex != -1)
            {
                visit = patientController.GetVisit(Convert.ToInt32(visitComboBox.SelectedValue));
                visitBindingSource.Add(visit);
                List<Employee> attendingDoctor = new List<Employee>();
                attendingDoctor.Add(adminController.GetEmployeeByID(visit.DoctorID));
                doctorIDComboBox.DataSource = attendingDoctor;
                doctorIDComboBox.DisplayMember = "FullName";
                doctorIDComboBox.ValueMember = "ID";
                doctorIDComboBox.SelectedValue = visit.DoctorID;
                visitTab.Enabled = false;
                txtNurseName.Text = adminController.GetEmployeeByID(visit.NurseID).FullName;
                diagnosisTab.Enabled = true;
                testsTab.Enabled = true;
                orgVisit = new Visit(visit);
                if (visit.FinalDiagnosisDate != null)
                {
                    diagnosisTab.Enabled = false;
                    testsTab.Enabled = false;
                }
                else if (String.IsNullOrEmpty(visit.InitialDiagnosis))
                {
                    initialDiagnosisTextBox.Enabled = true;
                    finalDiagnosisTextBox.Enabled = false;
                    loadTestNameComboBox();
                }
                else
                {
                    initialDiagnosisTextBox.Enabled = false;
                    finalDiagnosisTextBox.Enabled = true;

                }

                this.displayTests();
            }


        }

        private void btnNewVisit_Click(object sender, EventArgs e)
        {
            visitBindingSource.Clear();
            visitTab.Enabled = true;
            tabControlPatient.SelectedTab = tabVisit;
            heightTextBox.Focus();
            btnNewVisit.Enabled = false;
            try
            {
                doctorList = adminController.getActiveEmployeeListByRole("Doctor");
                doctorIDComboBox.DataSource = doctorList;
                doctorIDComboBox.DisplayMember = "FullName";
                doctorIDComboBox.ValueMember = "ID";
                doctorIDComboBox.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            visit = new Visit();
            visitBindingSource.Add(visit);
            visit.NurseID = NurseLoggedIn.ID;
            txtNurseName.Text = NurseLoggedIn.FullName;
            
        }

        private void btnAddVisit_Click(object sender, EventArgs e)
        {
            if (Validator.IsDecimal(heightTextBox, 2) && Validator.IsDecimalInRange(heightTextBox, 15, 120) &&
                Validator.IsDecimal(weightTextBox, 2) && Validator.IsDecimalInRange(weightTextBox, 5, 800) &&
                Validator.IsInt32(bloodPressureSystolicTextBox) && Validator.IsIntegerInRange(bloodPressureSystolicTextBox, 40, 250) &&
                Validator.IsInt32(bloodPressureDiastolicTextBox) && Validator.IsIntegerInRange(bloodPressureDiastolicTextBox, 20, 200) &&
                Validator.IsPresent(pulseTextBox) && Validator.IsNumberInRange(pulseTextBox, 35, 250) &&
                Validator.IsDecimal(temperatureTextBox, 2) && Validator.IsDecimalInRange(temperatureTextBox, 92, 106) &&
                Validator.IsPresent(symptomsTextBox) &&
                Validator.IsPresent(doctorIDComboBox))
            {
                try
                {
                    visit.PatientID = patient.ID;
                    visit.DoctorID = Convert.ToInt32(doctorIDComboBox.SelectedValue);
                    visit.ID = patientController.CreateVisit(visit);
                    MessageBox.Show("Visit was successfully added");

                    // Add new visit to the combobox and then select that visit
                    this.loadVisits(patient.ID);
                    this.displayVisit();
                    btnNewVisit.Enabled = true;
                   
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }

            }
        }

        private void btnCancelVisit_Click(object sender, EventArgs e)
        {
            this.displayVisit();
            btnNewVisit.Enabled = true;
            visitComboBox.Enabled = true;
        }

        private void iDTextBox_TextChanged(object sender, EventArgs e)
        {

            resetPatientForm();
        }

        private void stateCodeComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            this.patient.StateCode = stateCodeComboBox.SelectedValue.ToString();
        }

        private void genderComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            this.patient.Gender = genderComboBox.SelectedItem.ToString();
        }

        private void visitComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            tabControlPatient.SelectedTab = tabVisit;
            displayVisit();
        }

        private void btnDiagnosisSave_Click(object sender, EventArgs e)
        {
            if (initialDiagnosisTextBox.Enabled && Validator.IsPresent(initialDiagnosisTextBox) ||
               (finalDiagnosisTextBox.Enabled && Validator.IsPresent(finalDiagnosisTextBox))) 
            {
                try
                {
                    if (!patientController.UpdateVisitDiagnosis(orgVisit, visit))
                    {
                        MessageBox.Show("Another user has updated " +
                            "that visit already.", "Database Error");
                        iDTextBox.Text = patient.ID.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Visit Diagnosis was updated successfully");
                        if (initialDiagnosisTextBox.Enabled)
                        {
                            initialDiagnosisTextBox.Enabled = false;
                            finalDiagnosisTextBox.Enabled = true;
                        }
                        else
                        {
                            diagnosisTab.Enabled = false;
                        }
                        displayVisit();
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        private void btnDiagnosisCancel_Click(object sender, EventArgs e)
        {
            this.displayVisit();
        }

        private void btnOrder_Click(object sender, EventArgs e)
        {
            if (patientController.OrderTest(
                visit.ID, Convert.ToInt32(testNameComboBox.SelectedValue)))
            {
                this.displayTests();
                loadTestNameComboBox();
            }
            else
                MessageBox.Show("Test Order was not successful", "Order Failed");
            

        }

        private void loadTestNameComboBox()
        {
            try
            {
                testList = patientController.GetActiveTestListNotOrdered(visit.ID);
                testNameComboBox.DataSource = testList;
                testNameComboBox.DisplayMember = "name";
                testNameComboBox.ValueMember = "id";
                testNameComboBox.SelectedIndex = -1;
                btnOrder.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void displayTests()
        {
            testOrderBindingSource.Clear();
           List<TestOrder> testOrderList =
                patientController.GetTestOrderListByVisitID(visit.ID);
            dgvTestOrders.DataSource = testOrderList;

        }

        private void btnUpdateResults_Click(object sender, EventArgs e)
        {
            bool isSuccessful = false;
            
            if (dgvTestOrders.RowCount > 0)
            {
                int orgRowIndex = dgvTestOrders.CurrentCell.RowIndex;
                orgTestOrder = new TestOrder(dgvTestOrders.Rows[orgRowIndex].DataBoundItem as TestOrder);
                int rowIndex = dgvTestOrders.CurrentCell.RowIndex;
                testOrder = dgvTestOrders.Rows[rowIndex].DataBoundItem as TestOrder;
                testOrder.DatePerformed = dtpDatePerformed.Value.Date;
                testOrder.Results = cboResults.Text.ToString();
            }
            else
            {
                MessageBox.Show("No test selected to Update", "No Test Selected");
                return;
            }
            if (String.IsNullOrEmpty(orgTestOrder.Results) && cboResults.SelectedIndex == -1)
            {
                MessageBox.Show("A Result Must be Selected", "Missing Entry");
                testOrder.DatePerformed = orgTestOrder.DatePerformed;
                return;
            }
            if (orgTestOrder.DatePerformed == null && !dtpDatePerformed.Checked)
            {
                MessageBox.Show("A Date Performed Must be Selected", "Missing Entry");
                testOrder.DatePerformed = orgTestOrder.DatePerformed;
                return;
            }
            if (String.IsNullOrEmpty(testOrder.Results))
                testOrder.Results = orgTestOrder.Results;

            if (!dtpDatePerformed.Checked)
                testOrder.DatePerformed = orgTestOrder.DatePerformed;

            if (visit.FinalDiagnosisDate == null)
                try
                {
                    isSuccessful = patientController.UpdateTestOrder(orgTestOrder, testOrder);
                    if (!isSuccessful)
                    {
                        MessageBox.Show("Another user has updated or " +
                        "deleted that TestOrder.", "Database Error");
                    }
                    else if (!isSuccessful && visit.FinalDiagnosis != null)
                    {
                        MessageBox.Show("Test Results Were Not Updated", "Error"); 
                    } 
                    else
                    {
                        MessageBox.Show("Test Results Updated Successfuly", "Successful");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            else
                MessageBox.Show("Can not Update after final diagnosis is made", "Visit Closed");
            
            cboResults.SelectedIndex = -1;
            this.displayTests();
        }


        private void dgvTestOrders_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                int rowIndex = e.RowIndex;
                TestOrder testOrder = dgvTestOrders.Rows[rowIndex].DataBoundItem as TestOrder;
                if (String.IsNullOrEmpty(testOrder.Results))
                {
                    try
                    {
                        if (patientController.DeleteTestOrder(testOrder))
                            loadTestNameComboBox();
                        else
                            MessageBox.Show(testOrder.TestName + " has been modified or removed by another user");
                        displayTests();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Cannot remove a test order once results are entered");
                }
            }
        }

        private void testNameComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            btnOrder.Enabled = true;
        }

        private void testNameComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            btnOrder.Enabled = true;
        }

        
       
    }
}
