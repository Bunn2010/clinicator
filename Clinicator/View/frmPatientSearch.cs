﻿using Clinicator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clinicator.Controller;
using Clinicator.Model;

namespace Clinicator.View
{
    /// <summary>
    /// Form for searching for and selecting existing patients
    /// </summary>
    public partial class frmPatientSearch : Form
    {
        private PatientController patientController = new PatientController();
        public Patient patient;
        private List<Patient> patientList;
        
        /// <summary>
        /// Initializes the PatientSearch form from the Patient form
        /// </summary>
        public frmPatientSearch()
        {
            InitializeComponent();
            dgvPatients.RowHeadersVisible = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text.Trim() == "" && txtLastName.Text.Trim() == "" && !dtpPatientDOB.Checked)
            {
                MessageBox.Show("No search criteria has been entered", "Search Criteria Missing");
                return;
            }

            patient = new Patient();
            patientList = new List<Patient>();

            patient.FirstName = txtFirstName.Text.Trim();
            patient.LastName = txtLastName.Text.Trim();
            if (dtpPatientDOB.Checked)
                patient.DOB = dtpPatientDOB.Value.Date;

            patientList = patientController.GetPatientsBySearchFields(patient);
            dgvPatients.DataSource = patientList;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void dgvPatients_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                int rowIndex = e.RowIndex;
                patient = dgvPatients.Rows[rowIndex].DataBoundItem as Patient;
                this.Visible = false;
            }
        }

        private void frmPatientSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.btnSearch_Click(this, e);
            }
        }
    }
}
