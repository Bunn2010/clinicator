﻿namespace Clinicator
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label activeLabel;
            System.Windows.Forms.Label sSNLabel;
            System.Windows.Forms.Label zipLabel;
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.patientVisitBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cS6232_G3DataSet = new Clinicator.CS6232_G3DataSet();
            this.tabControlAdmin = new System.Windows.Forms.TabControl();
            this.tabEmployees = new System.Windows.Forms.TabPage();
            this.zipMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.employeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sSNMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.stateCodeComboBox = new System.Windows.Forms.ComboBox();
            this.stateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.activeComboBox = new System.Windows.Forms.ComboBox();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btnNewEmployee = new System.Windows.Forms.Button();
            this.grpSystemAccess = new System.Windows.Forms.GroupBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.roleComboBox = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnRetrieve = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.iDTextBox = new System.Windows.Forms.TextBox();
            this.btnEmployeeCancel = new System.Windows.Forms.Button();
            this.btnEmployeeSave = new System.Windows.Forms.Button();
            this.dOBDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.address2TextBox = new System.Windows.Forms.TextBox();
            this.address1TextBox = new System.Windows.Forms.TextBox();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.middleInitialTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.genderComboBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabTests = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtTestName = new System.Windows.Forms.TextBox();
            this.btnAddTest = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboTestActive = new System.Windows.Forms.ComboBox();
            this.chkTestNameContains = new System.Windows.Forms.CheckBox();
            this.testSearch = new System.Windows.Forms.Button();
            this.txtSearchTest = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dgvTests = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateBtn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabReport = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.btnGenerateReport = new System.Windows.Forms.Button();
            this.dtpBegin = new System.Windows.Forms.DateTimePicker();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.patientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.patientVisitTableAdapter = new Clinicator.CS6232_G3DataSetTableAdapters.PatientVisitTableAdapter();
            activeLabel = new System.Windows.Forms.Label();
            sSNLabel = new System.Windows.Forms.Label();
            zipLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.patientVisitBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cS6232_G3DataSet)).BeginInit();
            this.tabControlAdmin.SuspendLayout();
            this.tabEmployees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).BeginInit();
            this.grpSystemAccess.SuspendLayout();
            this.tabTests.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.tabReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // activeLabel
            // 
            activeLabel.AutoSize = true;
            activeLabel.Location = new System.Drawing.Point(209, 288);
            activeLabel.Name = "activeLabel";
            activeLabel.Size = new System.Drawing.Size(40, 13);
            activeLabel.TabIndex = 35;
            activeLabel.Text = "Active:";
            // 
            // sSNLabel
            // 
            sSNLabel.AutoSize = true;
            sSNLabel.Location = new System.Drawing.Point(46, 106);
            sSNLabel.Name = "sSNLabel";
            sSNLabel.Size = new System.Drawing.Size(32, 13);
            sSNLabel.TabIndex = 37;
            sSNLabel.Text = "SSN:";
            // 
            // zipLabel
            // 
            zipLabel.AutoSize = true;
            zipLabel.Location = new System.Drawing.Point(444, 231);
            zipLabel.Name = "zipLabel";
            zipLabel.Size = new System.Drawing.Size(25, 13);
            zipLabel.TabIndex = 38;
            zipLabel.Text = "Zip:";
            // 
            // patientVisitBindingSource
            // 
            this.patientVisitBindingSource.DataMember = "PatientVisit";
            this.patientVisitBindingSource.DataSource = this.cS6232_G3DataSet;
            // 
            // cS6232_G3DataSet
            // 
            this.cS6232_G3DataSet.DataSetName = "CS6232_G3DataSet";
            this.cS6232_G3DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabControlAdmin
            // 
            this.tabControlAdmin.Controls.Add(this.tabEmployees);
            this.tabControlAdmin.Controls.Add(this.tabTests);
            this.tabControlAdmin.Controls.Add(this.tabReport);
            this.tabControlAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlAdmin.Location = new System.Drawing.Point(0, 0);
            this.tabControlAdmin.Name = "tabControlAdmin";
            this.tabControlAdmin.SelectedIndex = 0;
            this.tabControlAdmin.Size = new System.Drawing.Size(800, 481);
            this.tabControlAdmin.TabIndex = 3;
            this.tabControlAdmin.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControlAdmin_Selected);
            // 
            // tabEmployees
            // 
            this.tabEmployees.AutoScroll = true;
            this.tabEmployees.Controls.Add(zipLabel);
            this.tabEmployees.Controls.Add(this.zipMaskedTextBox);
            this.tabEmployees.Controls.Add(sSNLabel);
            this.tabEmployees.Controls.Add(this.sSNMaskedTextBox);
            this.tabEmployees.Controls.Add(this.stateCodeComboBox);
            this.tabEmployees.Controls.Add(activeLabel);
            this.tabEmployees.Controls.Add(this.activeComboBox);
            this.tabEmployees.Controls.Add(this.phoneTextBox);
            this.tabEmployees.Controls.Add(this.label33);
            this.tabEmployees.Controls.Add(this.btnNewEmployee);
            this.tabEmployees.Controls.Add(this.grpSystemAccess);
            this.tabEmployees.Controls.Add(this.label3);
            this.tabEmployees.Controls.Add(this.roleComboBox);
            this.tabEmployees.Controls.Add(this.btnSearch);
            this.tabEmployees.Controls.Add(this.btnRetrieve);
            this.tabEmployees.Controls.Add(this.label1);
            this.tabEmployees.Controls.Add(this.iDTextBox);
            this.tabEmployees.Controls.Add(this.btnEmployeeCancel);
            this.tabEmployees.Controls.Add(this.btnEmployeeSave);
            this.tabEmployees.Controls.Add(this.dOBDateTimePicker);
            this.tabEmployees.Controls.Add(this.label20);
            this.tabEmployees.Controls.Add(this.cityTextBox);
            this.tabEmployees.Controls.Add(this.address2TextBox);
            this.tabEmployees.Controls.Add(this.address1TextBox);
            this.tabEmployees.Controls.Add(this.lastNameTextBox);
            this.tabEmployees.Controls.Add(this.middleInitialTextBox);
            this.tabEmployees.Controls.Add(this.firstNameTextBox);
            this.tabEmployees.Controls.Add(this.genderComboBox);
            this.tabEmployees.Controls.Add(this.label18);
            this.tabEmployees.Controls.Add(this.label16);
            this.tabEmployees.Controls.Add(this.label15);
            this.tabEmployees.Controls.Add(this.label14);
            this.tabEmployees.Controls.Add(this.label13);
            this.tabEmployees.Controls.Add(this.label12);
            this.tabEmployees.Controls.Add(this.label11);
            this.tabEmployees.Controls.Add(this.label10);
            this.tabEmployees.Controls.Add(this.label2);
            this.tabEmployees.Location = new System.Drawing.Point(4, 22);
            this.tabEmployees.Name = "tabEmployees";
            this.tabEmployees.Size = new System.Drawing.Size(792, 455);
            this.tabEmployees.TabIndex = 3;
            this.tabEmployees.Text = "Employees";
            this.tabEmployees.UseVisualStyleBackColor = true;
            // 
            // zipMaskedTextBox
            // 
            this.zipMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "Zip", true));
            this.zipMaskedTextBox.Location = new System.Drawing.Point(475, 228);
            this.zipMaskedTextBox.Mask = "00000";
            this.zipMaskedTextBox.Name = "zipMaskedTextBox";
            this.zipMaskedTextBox.Size = new System.Drawing.Size(44, 20);
            this.zipMaskedTextBox.TabIndex = 12;
            this.zipMaskedTextBox.Tag = "Zip Code";
            // 
            // employeeBindingSource
            // 
            this.employeeBindingSource.DataSource = typeof(Clinicator.Model.Employee);
            // 
            // sSNMaskedTextBox
            // 
            this.sSNMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "SSN", true));
            this.sSNMaskedTextBox.Location = new System.Drawing.Point(84, 103);
            this.sSNMaskedTextBox.Mask = "000-00-0000";
            this.sSNMaskedTextBox.Name = "sSNMaskedTextBox";
            this.sSNMaskedTextBox.Size = new System.Drawing.Size(72, 20);
            this.sSNMaskedTextBox.TabIndex = 6;
            this.sSNMaskedTextBox.Tag = "SSN";
            this.sSNMaskedTextBox.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // stateCodeComboBox
            // 
            this.stateCodeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "StateCode", true));
            this.stateCodeComboBox.DataSource = this.stateBindingSource;
            this.stateCodeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateCodeComboBox.FormattingEnabled = true;
            this.stateCodeComboBox.Location = new System.Drawing.Point(385, 228);
            this.stateCodeComboBox.Name = "stateCodeComboBox";
            this.stateCodeComboBox.Size = new System.Drawing.Size(41, 21);
            this.stateCodeComboBox.TabIndex = 11;
            this.stateCodeComboBox.Tag = "State";
            // 
            // stateBindingSource
            // 
            this.stateBindingSource.DataSource = typeof(Clinicator.Model.State);
            // 
            // activeComboBox
            // 
            this.activeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "Active", true));
            this.activeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.activeComboBox.FormattingEnabled = true;
            this.activeComboBox.Location = new System.Drawing.Point(251, 285);
            this.activeComboBox.Name = "activeComboBox";
            this.activeComboBox.Size = new System.Drawing.Size(40, 21);
            this.activeComboBox.TabIndex = 15;
            this.activeComboBox.Tag = "Active";
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "Phone", true));
            this.phoneTextBox.Location = new System.Drawing.Point(84, 257);
            this.phoneTextBox.MaxLength = 50;
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.Size = new System.Drawing.Size(103, 20);
            this.phoneTextBox.TabIndex = 13;
            this.phoneTextBox.Tag = "Phone";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(37, 260);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 13);
            this.label33.TabIndex = 34;
            this.label33.Text = "Phone:";
            // 
            // btnNewEmployee
            // 
            this.btnNewEmployee.Location = new System.Drawing.Point(314, 16);
            this.btnNewEmployee.Name = "btnNewEmployee";
            this.btnNewEmployee.Size = new System.Drawing.Size(75, 23);
            this.btnNewEmployee.TabIndex = 32;
            this.btnNewEmployee.TabStop = false;
            this.btnNewEmployee.Text = "New";
            this.btnNewEmployee.UseVisualStyleBackColor = true;
            this.btnNewEmployee.Click += new System.EventHandler(this.btnNewEmployee_Click);
            // 
            // grpSystemAccess
            // 
            this.grpSystemAccess.Controls.Add(this.txtPassword);
            this.grpSystemAccess.Controls.Add(this.label9);
            this.grpSystemAccess.Controls.Add(this.txtUserID);
            this.grpSystemAccess.Controls.Add(this.label8);
            this.grpSystemAccess.Controls.Add(this.label4);
            this.grpSystemAccess.Location = new System.Drawing.Point(40, 312);
            this.grpSystemAccess.Name = "grpSystemAccess";
            this.grpSystemAccess.Size = new System.Drawing.Size(268, 100);
            this.grpSystemAccess.TabIndex = 31;
            this.grpSystemAccess.TabStop = false;
            this.grpSystemAccess.Text = "System Access*:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(76, 59);
            this.txtPassword.MaxLength = 20;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(161, 20);
            this.txtPassword.TabIndex = 17;
            this.txtPassword.Tag = "Password";
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Password:";
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(76, 29);
            this.txtUserID.MaxLength = 20;
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(161, 20);
            this.txtUserID.TabIndex = 16;
            this.txtUserID.Tag = "User ID";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "User ID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(206, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "*Currently only available to Nurse or Admin";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 288);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Role:";
            // 
            // roleComboBox
            // 
            this.roleComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "Role", true));
            this.roleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.roleComboBox.FormattingEnabled = true;
            this.roleComboBox.ItemHeight = 13;
            this.roleComboBox.Location = new System.Drawing.Point(84, 285);
            this.roleComboBox.Name = "roleComboBox";
            this.roleComboBox.Size = new System.Drawing.Size(112, 21);
            this.roleComboBox.TabIndex = 14;
            this.roleComboBox.Tag = "Role";
            this.roleComboBox.SelectionChangeCommitted += new System.EventHandler(this.roleComboBox_SelectionChangeCommitted);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(233, 16);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 28;
            this.btnSearch.TabStop = false;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnRetrieve
            // 
            this.btnRetrieve.Location = new System.Drawing.Point(149, 16);
            this.btnRetrieve.Name = "btnRetrieve";
            this.btnRetrieve.Size = new System.Drawing.Size(75, 23);
            this.btnRetrieve.TabIndex = 27;
            this.btnRetrieve.TabStop = false;
            this.btnRetrieve.Text = "Retreive";
            this.btnRetrieve.UseVisualStyleBackColor = true;
            this.btnRetrieve.Click += new System.EventHandler(this.btnRetrieve_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Employee ID:";
            // 
            // iDTextBox
            // 
            this.iDTextBox.Location = new System.Drawing.Point(84, 18);
            this.iDTextBox.MaxLength = 9;
            this.iDTextBox.Name = "iDTextBox";
            this.iDTextBox.Size = new System.Drawing.Size(49, 20);
            this.iDTextBox.TabIndex = 0;
            this.iDTextBox.Tag = "ID";
            this.iDTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.iDTextBox.TextChanged += new System.EventHandler(this.iDTextBox_TextChanged);
            this.iDTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.iDTextBox_KeyDown);
            // 
            // btnEmployeeCancel
            // 
            this.btnEmployeeCancel.Location = new System.Drawing.Point(500, 384);
            this.btnEmployeeCancel.Name = "btnEmployeeCancel";
            this.btnEmployeeCancel.Size = new System.Drawing.Size(75, 23);
            this.btnEmployeeCancel.TabIndex = 19;
            this.btnEmployeeCancel.Text = "Cancel";
            this.btnEmployeeCancel.UseVisualStyleBackColor = true;
            this.btnEmployeeCancel.Click += new System.EventHandler(this.btnEmployeeCancel_Click);
            // 
            // btnEmployeeSave
            // 
            this.btnEmployeeSave.AutoSize = true;
            this.btnEmployeeSave.Location = new System.Drawing.Point(410, 384);
            this.btnEmployeeSave.Name = "btnEmployeeSave";
            this.btnEmployeeSave.Size = new System.Drawing.Size(84, 23);
            this.btnEmployeeSave.TabIndex = 18;
            this.btnEmployeeSave.Text = "Save";
            this.btnEmployeeSave.UseVisualStyleBackColor = true;
            this.btnEmployeeSave.Click += new System.EventHandler(this.btnEmployeeSave_Click);
            // 
            // dOBDateTimePicker
            // 
            this.dOBDateTimePicker.Cursor = System.Windows.Forms.Cursors.Default;
            this.dOBDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.employeeBindingSource, "DOB", true));
            this.dOBDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dOBDateTimePicker.Location = new System.Drawing.Point(84, 129);
            this.dOBDateTimePicker.Name = "dOBDateTimePicker";
            this.dOBDateTimePicker.ShowCheckBox = true;
            this.dOBDateTimePicker.Size = new System.Drawing.Size(103, 20);
            this.dOBDateTimePicker.TabIndex = 7;
            this.dOBDateTimePicker.Tag = "Date of Birth";
            this.dOBDateTimePicker.Value = new System.DateTime(2015, 4, 2, 0, 0, 0, 0);
            this.dOBDateTimePicker.ValueChanged += new System.EventHandler(this.dOBDateTimePicker_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 135);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "Date of Birth:";
            // 
            // cityTextBox
            // 
            this.cityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "City", true));
            this.cityTextBox.Location = new System.Drawing.Point(84, 228);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(254, 20);
            this.cityTextBox.TabIndex = 10;
            this.cityTextBox.Tag = "City";
            // 
            // address2TextBox
            // 
            this.address2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "Address2", true));
            this.address2TextBox.Location = new System.Drawing.Point(84, 196);
            this.address2TextBox.Name = "address2TextBox";
            this.address2TextBox.Size = new System.Drawing.Size(254, 20);
            this.address2TextBox.TabIndex = 9;
            this.address2TextBox.Tag = "Address2";
            // 
            // address1TextBox
            // 
            this.address1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "Address1", true));
            this.address1TextBox.Location = new System.Drawing.Point(84, 164);
            this.address1TextBox.Name = "address1TextBox";
            this.address1TextBox.Size = new System.Drawing.Size(254, 20);
            this.address1TextBox.TabIndex = 8;
            this.address1TextBox.Tag = "Address1";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "LastName", true));
            this.lastNameTextBox.Location = new System.Drawing.Point(285, 71);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(161, 20);
            this.lastNameTextBox.TabIndex = 4;
            this.lastNameTextBox.Tag = "Last Name";
            // 
            // middleInitialTextBox
            // 
            this.middleInitialTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.middleInitialTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "MiddleInitial", true));
            this.middleInitialTextBox.Location = new System.Drawing.Point(252, 71);
            this.middleInitialTextBox.Name = "middleInitialTextBox";
            this.middleInitialTextBox.Size = new System.Drawing.Size(28, 20);
            this.middleInitialTextBox.TabIndex = 3;
            this.middleInitialTextBox.Tag = "Middle Initial";
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "FirstName", true));
            this.firstNameTextBox.Location = new System.Drawing.Point(84, 71);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(161, 20);
            this.firstNameTextBox.TabIndex = 2;
            this.firstNameTextBox.Tag = "First Name";
            // 
            // genderComboBox
            // 
            this.genderComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeeBindingSource, "Gender", true));
            this.genderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.genderComboBox.FormattingEnabled = true;
            this.genderComboBox.Location = new System.Drawing.Point(500, 71);
            this.genderComboBox.Name = "genderComboBox";
            this.genderComboBox.Size = new System.Drawing.Size(35, 21);
            this.genderComboBox.TabIndex = 5;
            this.genderComboBox.Tag = "Gender";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(455, 74);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "Gender:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(344, 231);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "State:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(51, 231);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "City:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 199);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Address 2:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 167);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Address 1:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(282, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Last";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(257, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "MI";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(81, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "First";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name:";
            // 
            // tabTests
            // 
            this.tabTests.Controls.Add(this.splitContainer1);
            this.tabTests.Location = new System.Drawing.Point(4, 22);
            this.tabTests.Name = "tabTests";
            this.tabTests.Padding = new System.Windows.Forms.Padding(3);
            this.tabTests.Size = new System.Drawing.Size(792, 455);
            this.tabTests.TabIndex = 2;
            this.tabTests.Text = "Tests";
            this.tabTests.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1MinSize = 50;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvTests);
            this.splitContainer1.Size = new System.Drawing.Size(786, 449);
            this.splitContainer1.SplitterDistance = 60;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtTestName);
            this.groupBox3.Controls.Add(this.btnAddTest);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Location = new System.Drawing.Point(476, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(296, 43);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "New Test";
            // 
            // txtTestName
            // 
            this.txtTestName.Location = new System.Drawing.Point(66, 13);
            this.txtTestName.MaxLength = 50;
            this.txtTestName.Name = "txtTestName";
            this.txtTestName.Size = new System.Drawing.Size(149, 20);
            this.txtTestName.TabIndex = 1;
            this.txtTestName.Tag = "New Test Name";
            this.txtTestName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTestName_KeyDown);
            // 
            // btnAddTest
            // 
            this.btnAddTest.Location = new System.Drawing.Point(219, 11);
            this.btnAddTest.Name = "btnAddTest";
            this.btnAddTest.Size = new System.Drawing.Size(75, 23);
            this.btnAddTest.TabIndex = 5;
            this.btnAddTest.Text = "Add";
            this.btnAddTest.UseVisualStyleBackColor = true;
            this.btnAddTest.Click += new System.EventHandler(this.btnAddTest_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cboTestActive);
            this.groupBox2.Controls.Add(this.chkTestNameContains);
            this.groupBox2.Controls.Add(this.testSearch);
            this.groupBox2.Controls.Add(this.txtSearchTest);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Location = new System.Drawing.Point(5, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(465, 43);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(283, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Active:";
            // 
            // cboTestActive
            // 
            this.cboTestActive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTestActive.FormattingEnabled = true;
            this.cboTestActive.Items.AddRange(new object[] {
            "All",
            "Y",
            "N"});
            this.cboTestActive.Location = new System.Drawing.Point(329, 12);
            this.cboTestActive.Name = "cboTestActive";
            this.cboTestActive.Size = new System.Drawing.Size(49, 21);
            this.cboTestActive.TabIndex = 6;
            // 
            // chkTestNameContains
            // 
            this.chkTestNameContains.AutoSize = true;
            this.chkTestNameContains.Location = new System.Drawing.Point(196, 15);
            this.chkTestNameContains.Name = "chkTestNameContains";
            this.chkTestNameContains.Size = new System.Drawing.Size(67, 17);
            this.chkTestNameContains.TabIndex = 5;
            this.chkTestNameContains.Text = "Contains";
            this.chkTestNameContains.UseVisualStyleBackColor = true;
            // 
            // testSearch
            // 
            this.testSearch.Location = new System.Drawing.Point(384, 11);
            this.testSearch.Name = "testSearch";
            this.testSearch.Size = new System.Drawing.Size(75, 23);
            this.testSearch.TabIndex = 4;
            this.testSearch.Text = "Search";
            this.testSearch.UseVisualStyleBackColor = true;
            this.testSearch.Click += new System.EventHandler(this.testSearch_Click);
            // 
            // txtSearchTest
            // 
            this.txtSearchTest.Location = new System.Drawing.Point(41, 13);
            this.txtSearchTest.Name = "txtSearchTest";
            this.txtSearchTest.Size = new System.Drawing.Size(149, 20);
            this.txtSearchTest.TabIndex = 1;
            this.txtSearchTest.Tag = "Search Name";
            this.txtSearchTest.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchTest_KeyDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 17);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(38, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Name:";
            // 
            // dgvTests
            // 
            this.dgvTests.AllowUserToAddRows = false;
            this.dgvTests.AllowUserToDeleteRows = false;
            this.dgvTests.AutoGenerateColumns = false;
            this.dgvTests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTests.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.activeDataGridViewTextBoxColumn,
            this.UpdateBtn});
            this.dgvTests.DataSource = this.testBindingSource;
            this.dgvTests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTests.Location = new System.Drawing.Point(0, 0);
            this.dgvTests.Name = "dgvTests";
            this.dgvTests.ReadOnly = true;
            this.dgvTests.Size = new System.Drawing.Size(786, 385);
            this.dgvTests.TabIndex = 28;
            this.dgvTests.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTests_CellContentClick);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Width = 43;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // activeDataGridViewTextBoxColumn
            // 
            this.activeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.activeDataGridViewTextBoxColumn.DataPropertyName = "Active";
            this.activeDataGridViewTextBoxColumn.HeaderText = "Active";
            this.activeDataGridViewTextBoxColumn.Name = "activeDataGridViewTextBoxColumn";
            this.activeDataGridViewTextBoxColumn.ReadOnly = true;
            this.activeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.activeDataGridViewTextBoxColumn.Width = 62;
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.HeaderText = "";
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.ReadOnly = true;
            this.UpdateBtn.Text = "Update";
            this.UpdateBtn.UseColumnTextForButtonValue = true;
            // 
            // testBindingSource
            // 
            this.testBindingSource.DataSource = typeof(Clinicator.Model.Test);
            // 
            // tabReport
            // 
            this.tabReport.Controls.Add(this.splitContainer2);
            this.tabReport.Location = new System.Drawing.Point(4, 22);
            this.tabReport.Name = "tabReport";
            this.tabReport.Padding = new System.Windows.Forms.Padding(3);
            this.tabReport.Size = new System.Drawing.Size(792, 455);
            this.tabReport.TabIndex = 4;
            this.tabReport.Text = "Report";
            this.tabReport.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer2.Panel1MinSize = 50;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.reportViewer1);
            this.splitContainer2.Size = new System.Drawing.Size(786, 449);
            this.splitContainer2.SplitterDistance = 60;
            this.splitContainer2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dtpEnd);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnGenerateReport);
            this.groupBox1.Controls.Add(this.dtpBegin);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(640, 51);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Patient Visits During Date Range";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Start:";
            // 
            // dtpEnd
            // 
            this.dtpEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEnd.Location = new System.Drawing.Point(209, 22);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(120, 20);
            this.dtpEnd.TabIndex = 31;
            this.dtpEnd.Tag = "End Date";
            this.dtpEnd.Value = new System.DateTime(2015, 4, 18, 21, 36, 15, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(174, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "End:";
            // 
            // btnGenerateReport
            // 
            this.btnGenerateReport.AutoSize = true;
            this.btnGenerateReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateReport.Location = new System.Drawing.Point(335, 21);
            this.btnGenerateReport.Name = "btnGenerateReport";
            this.btnGenerateReport.Size = new System.Drawing.Size(75, 23);
            this.btnGenerateReport.TabIndex = 26;
            this.btnGenerateReport.Text = "Generate";
            this.btnGenerateReport.UseVisualStyleBackColor = true;
            this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
            // 
            // dtpBegin
            // 
            this.dtpBegin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpBegin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBegin.Location = new System.Drawing.Point(48, 22);
            this.dtpBegin.Name = "dtpBegin";
            this.dtpBegin.Size = new System.Drawing.Size(120, 20);
            this.dtpBegin.TabIndex = 30;
            this.dtpBegin.Tag = "Start Date";
            this.dtpBegin.Value = new System.DateTime(1955, 5, 5, 0, 0, 0, 0);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "PatientVisitDataSet";
            reportDataSource1.Value = this.patientVisitBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Clinicator.PatientVisitReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(786, 385);
            this.reportViewer1.TabIndex = 35;
            this.reportViewer1.ReportRefresh += new System.ComponentModel.CancelEventHandler(this.reportViewer1_ReportRefresh);
            // 
            // patientBindingSource
            // 
            this.patientBindingSource.DataSource = typeof(Clinicator.Model.Patient);
            // 
            // patientVisitTableAdapter
            // 
            this.patientVisitTableAdapter.ClearBeforeFill = true;
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(800, 481);
            this.ControlBox = false;
            this.Controls.Add(this.tabControlAdmin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAdmin";
            this.Text = "Admin";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.handleEmployeeSearch);
            this.Load += new System.EventHandler(this.frmAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.patientVisitBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cS6232_G3DataSet)).EndInit();
            this.tabControlAdmin.ResumeLayout(false);
            this.tabEmployees.ResumeLayout(false);
            this.tabEmployees.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).EndInit();
            this.grpSystemAccess.ResumeLayout(false);
            this.grpSystemAccess.PerformLayout();
            this.tabTests.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.tabReport.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlAdmin;
        private System.Windows.Forms.TabPage tabEmployees;
        private System.Windows.Forms.GroupBox grpSystemAccess;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox roleComboBox;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnRetrieve;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox iDTextBox;
        private System.Windows.Forms.Button btnEmployeeCancel;
        private System.Windows.Forms.Button btnEmployeeSave;
        private System.Windows.Forms.DateTimePicker dOBDateTimePicker;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.TextBox address2TextBox;
        private System.Windows.Forms.TextBox address1TextBox;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox middleInitialTextBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.ComboBox genderComboBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabTests;
        private System.Windows.Forms.Button btnNewEmployee;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button testSearch;
        private System.Windows.Forms.TextBox txtSearchTest;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView dgvTests;
        private System.Windows.Forms.BindingSource employeeBindingSource;
        private System.Windows.Forms.ComboBox activeComboBox;
        private System.Windows.Forms.BindingSource stateBindingSource;
        private System.Windows.Forms.ComboBox stateCodeComboBox;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.BindingSource patientBindingSource;
        private System.Windows.Forms.MaskedTextBox zipMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox sSNMaskedTextBox;
        private System.Windows.Forms.BindingSource testBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn UpdateBtn;
        private System.Windows.Forms.Button btnAddTest;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtTestName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.BindingSource patientVisitBindingSource;
        private CS6232_G3DataSet cS6232_G3DataSet;
        private CS6232_G3DataSetTableAdapters.PatientVisitTableAdapter patientVisitTableAdapter;
        private System.Windows.Forms.TabPage tabReport;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGenerateReport;
        private System.Windows.Forms.DateTimePicker dtpBegin;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.CheckBox chkTestNameContains;
        private System.Windows.Forms.ComboBox cboTestActive;
        private System.Windows.Forms.Label label7;
    }
}