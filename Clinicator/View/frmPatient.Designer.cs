﻿namespace Clinicator
{
    partial class frmPatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblState;
            System.Windows.Forms.Label firstNameLabel;
            System.Windows.Forms.Label bloodPressureDiastolicLabel;
            System.Windows.Forms.Label bloodPressureSystolicLabel;
            System.Windows.Forms.Label heightLabel;
            System.Windows.Forms.Label weightLabel;
            System.Windows.Forms.Label pulseLabel;
            System.Windows.Forms.Label temperatureLabel;
            System.Windows.Forms.Label symptomsLabel;
            System.Windows.Forms.Label initialDiagnosisLabel;
            System.Windows.Forms.Label finalDiagnosisLabel;
            this.dOBDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.patientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnNewVisit = new System.Windows.Forms.Button();
            this.visitComboBox = new System.Windows.Forms.ComboBox();
            this.visitBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.patientNameTextBox = new System.Windows.Forms.TextBox();
            this.iDTextBox = new System.Windows.Forms.TextBox();
            this.btnRetrieve = new System.Windows.Forms.Button();
            this.btnNewPatient = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControlPatient = new System.Windows.Forms.TabControl();
            this.tabRegistration = new System.Windows.Forms.TabPage();
            this.sSNMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.stateCodeComboBox = new System.Windows.Forms.ComboBox();
            this.stateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.genderComboBox = new System.Windows.Forms.ComboBox();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.zipTextBox = new System.Windows.Forms.TextBox();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.address2TextBox = new System.Windows.Forms.TextBox();
            this.address1TextBox = new System.Windows.Forms.TextBox();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.middleInitialTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btnRegistrationCancel = new System.Windows.Forms.Button();
            this.btnRegistrationSave = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabVisit = new System.Windows.Forms.TabPage();
            this.doctorIDComboBox = new System.Windows.Forms.ComboBox();
            this.symptomsTextBox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtNurseName = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pulseTextBox = new System.Windows.Forms.TextBox();
            this.temperatureTextBox = new System.Windows.Forms.TextBox();
            this.weightTextBox = new System.Windows.Forms.TextBox();
            this.heightTextBox = new System.Windows.Forms.TextBox();
            this.bloodPressureSystolicTextBox = new System.Windows.Forms.TextBox();
            this.bloodPressureDiastolicTextBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.btnCancelVisit = new System.Windows.Forms.Button();
            this.btnAddVisit = new System.Windows.Forms.Button();
            this.tabDiagnosis = new System.Windows.Forms.TabPage();
            this.initialDiagnosisTextBox = new System.Windows.Forms.TextBox();
            this.btnDiagnosisCancel = new System.Windows.Forms.Button();
            this.btnDiagnosisSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.finalDiagnosisDateTextBox = new System.Windows.Forms.TextBox();
            this.finalDiagnosisTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabTests = new System.Windows.Forms.TabPage();
            this.testNameComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cboResults = new System.Windows.Forms.ComboBox();
            this.btnUpdateResults = new System.Windows.Forms.Button();
            this.dtpDatePerformed = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnOrder = new System.Windows.Forms.Button();
            this.dgvTestOrders = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnTestOrderRemove = new System.Windows.Forms.DataGridViewButtonColumn();
            this.visitIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testOrderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            lblState = new System.Windows.Forms.Label();
            firstNameLabel = new System.Windows.Forms.Label();
            bloodPressureDiastolicLabel = new System.Windows.Forms.Label();
            bloodPressureSystolicLabel = new System.Windows.Forms.Label();
            heightLabel = new System.Windows.Forms.Label();
            weightLabel = new System.Windows.Forms.Label();
            pulseLabel = new System.Windows.Forms.Label();
            temperatureLabel = new System.Windows.Forms.Label();
            symptomsLabel = new System.Windows.Forms.Label();
            initialDiagnosisLabel = new System.Windows.Forms.Label();
            finalDiagnosisLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visitBindingSource)).BeginInit();
            this.tabControlPatient.SuspendLayout();
            this.tabRegistration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).BeginInit();
            this.tabVisit.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabDiagnosis.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabTests.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTestOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testOrderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lblState
            // 
            lblState.AutoSize = true;
            lblState.CausesValidation = false;
            lblState.Location = new System.Drawing.Point(357, 200);
            lblState.Name = "lblState";
            lblState.Size = new System.Drawing.Size(35, 13);
            lblState.TabIndex = 27;
            lblState.Text = "State:";
            // 
            // firstNameLabel
            // 
            firstNameLabel.AutoSize = true;
            firstNameLabel.CausesValidation = false;
            firstNameLabel.Location = new System.Drawing.Point(19, 43);
            firstNameLabel.Name = "firstNameLabel";
            firstNameLabel.Size = new System.Drawing.Size(60, 13);
            firstNameLabel.TabIndex = 28;
            firstNameLabel.Text = "First Name:";
            // 
            // bloodPressureDiastolicLabel
            // 
            bloodPressureDiastolicLabel.AutoSize = true;
            bloodPressureDiastolicLabel.Location = new System.Drawing.Point(6, 94);
            bloodPressureDiastolicLabel.Name = "bloodPressureDiastolicLabel";
            bloodPressureDiastolicLabel.Size = new System.Drawing.Size(81, 13);
            bloodPressureDiastolicLabel.TabIndex = 38;
            bloodPressureDiastolicLabel.Text = "Blood Pressure:";
            // 
            // bloodPressureSystolicLabel
            // 
            bloodPressureSystolicLabel.AutoSize = true;
            bloodPressureSystolicLabel.Location = new System.Drawing.Point(130, 94);
            bloodPressureSystolicLabel.Name = "bloodPressureSystolicLabel";
            bloodPressureSystolicLabel.Size = new System.Drawing.Size(28, 13);
            bloodPressureSystolicLabel.TabIndex = 39;
            bloodPressureSystolicLabel.Text = "over";
            // 
            // heightLabel
            // 
            heightLabel.AutoSize = true;
            heightLabel.Location = new System.Drawing.Point(41, 22);
            heightLabel.Name = "heightLabel";
            heightLabel.Size = new System.Drawing.Size(41, 13);
            heightLabel.TabIndex = 40;
            heightLabel.Text = "Height:";
            // 
            // weightLabel
            // 
            weightLabel.AutoSize = true;
            weightLabel.Location = new System.Drawing.Point(38, 55);
            weightLabel.Name = "weightLabel";
            weightLabel.Size = new System.Drawing.Size(44, 13);
            weightLabel.TabIndex = 41;
            weightLabel.Text = "Weight:";
            // 
            // pulseLabel
            // 
            pulseLabel.AutoSize = true;
            pulseLabel.Location = new System.Drawing.Point(46, 130);
            pulseLabel.Name = "pulseLabel";
            pulseLabel.Size = new System.Drawing.Size(36, 13);
            pulseLabel.TabIndex = 42;
            pulseLabel.Text = "Pulse:";
            // 
            // temperatureLabel
            // 
            temperatureLabel.AutoSize = true;
            temperatureLabel.Location = new System.Drawing.Point(12, 169);
            temperatureLabel.Name = "temperatureLabel";
            temperatureLabel.Size = new System.Drawing.Size(70, 13);
            temperatureLabel.TabIndex = 43;
            temperatureLabel.Text = "Temperature:";
            // 
            // symptomsLabel
            // 
            symptomsLabel.AutoSize = true;
            symptomsLabel.Location = new System.Drawing.Point(249, 28);
            symptomsLabel.Name = "symptomsLabel";
            symptomsLabel.Size = new System.Drawing.Size(58, 13);
            symptomsLabel.TabIndex = 43;
            symptomsLabel.Text = "Symptoms:";
            // 
            // initialDiagnosisLabel
            // 
            initialDiagnosisLabel.AutoSize = true;
            initialDiagnosisLabel.Location = new System.Drawing.Point(6, 19);
            initialDiagnosisLabel.Name = "initialDiagnosisLabel";
            initialDiagnosisLabel.Size = new System.Drawing.Size(83, 13);
            initialDiagnosisLabel.TabIndex = 27;
            initialDiagnosisLabel.Text = "Initial Diagnosis:";
            // 
            // finalDiagnosisLabel
            // 
            finalDiagnosisLabel.AutoSize = true;
            finalDiagnosisLabel.Location = new System.Drawing.Point(17, 25);
            finalDiagnosisLabel.Name = "finalDiagnosisLabel";
            finalDiagnosisLabel.Size = new System.Drawing.Size(63, 13);
            finalDiagnosisLabel.TabIndex = 27;
            finalDiagnosisLabel.Text = "Description:";
            // 
            // dOBDateTimePicker
            // 
            this.dOBDateTimePicker.CausesValidation = false;
            this.dOBDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.patientBindingSource, "DOB", true));
            this.dOBDateTimePicker.Enabled = false;
            this.dOBDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dOBDateTimePicker.Location = new System.Drawing.Point(85, 98);
            this.dOBDateTimePicker.Name = "dOBDateTimePicker";
            this.dOBDateTimePicker.ShowCheckBox = true;
            this.dOBDateTimePicker.Size = new System.Drawing.Size(115, 20);
            this.dOBDateTimePicker.TabIndex = 15;
            this.dOBDateTimePicker.Tag = "Date of Birth";
            this.dOBDateTimePicker.ValueChanged += new System.EventHandler(this.dOBDateTimePicker_ValueChanged);
            // 
            // patientBindingSource
            // 
            this.patientBindingSource.DataSource = typeof(Clinicator.Model.Patient);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.btnNewVisit);
            this.splitContainer1.Panel1.Controls.Add(this.visitComboBox);
            this.splitContainer1.Panel1.Controls.Add(this.patientNameTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.iDTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.btnRetrieve);
            this.splitContainer1.Panel1.Controls.Add(this.btnNewPatient);
            this.splitContainer1.Panel1.Controls.Add(this.btnSearch);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1MinSize = 75;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControlPatient);
            this.splitContainer1.Size = new System.Drawing.Size(701, 475);
            this.splitContainer1.SplitterDistance = 90;
            this.splitContainer1.TabIndex = 0;
            // 
            // btnNewVisit
            // 
            this.btnNewVisit.AutoSize = true;
            this.btnNewVisit.Location = new System.Drawing.Point(602, 39);
            this.btnNewVisit.Name = "btnNewVisit";
            this.btnNewVisit.Size = new System.Drawing.Size(87, 23);
            this.btnNewVisit.TabIndex = 33;
            this.btnNewVisit.Text = "New Visit";
            this.btnNewVisit.UseVisualStyleBackColor = true;
            this.btnNewVisit.Click += new System.EventHandler(this.btnNewVisit_Click);
            // 
            // visitComboBox
            // 
            this.visitComboBox.DataSource = this.visitBindingSource;
            this.visitComboBox.DisplayMember = "Datetime";
            this.visitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.visitComboBox.Enabled = false;
            this.visitComboBox.FormatString = "G";
            this.visitComboBox.FormattingEnabled = true;
            this.visitComboBox.Location = new System.Drawing.Point(453, 40);
            this.visitComboBox.Name = "visitComboBox";
            this.visitComboBox.Size = new System.Drawing.Size(143, 21);
            this.visitComboBox.TabIndex = 32;
            this.visitComboBox.SelectionChangeCommitted += new System.EventHandler(this.visitComboBox_SelectionChangeCommitted);
            // 
            // visitBindingSource
            // 
            this.visitBindingSource.DataSource = typeof(Clinicator.Model.Visit);
            // 
            // patientNameTextBox
            // 
            this.patientNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "PatientName", true));
            this.patientNameTextBox.Enabled = false;
            this.patientNameTextBox.Location = new System.Drawing.Point(89, 41);
            this.patientNameTextBox.Name = "patientNameTextBox";
            this.patientNameTextBox.ReadOnly = true;
            this.patientNameTextBox.Size = new System.Drawing.Size(275, 20);
            this.patientNameTextBox.TabIndex = 31;
            this.patientNameTextBox.TabStop = false;
            // 
            // iDTextBox
            // 
            this.iDTextBox.Location = new System.Drawing.Point(89, 9);
            this.iDTextBox.MaxLength = 9;
            this.iDTextBox.Name = "iDTextBox";
            this.iDTextBox.Size = new System.Drawing.Size(49, 20);
            this.iDTextBox.TabIndex = 1;
            this.iDTextBox.Tag = "Patient ID";
            this.iDTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.iDTextBox.TextChanged += new System.EventHandler(this.iDTextBox_TextChanged);
            this.iDTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.iDTextBox_KeyDown);
            // 
            // btnRetrieve
            // 
            this.btnRetrieve.AutoSize = true;
            this.btnRetrieve.Location = new System.Drawing.Point(153, 9);
            this.btnRetrieve.Name = "btnRetrieve";
            this.btnRetrieve.Size = new System.Drawing.Size(87, 23);
            this.btnRetrieve.TabIndex = 2;
            this.btnRetrieve.Text = "Retrieve";
            this.btnRetrieve.UseVisualStyleBackColor = true;
            this.btnRetrieve.Click += new System.EventHandler(this.btnRetrieve_Click);
            // 
            // btnNewPatient
            // 
            this.btnNewPatient.AutoSize = true;
            this.btnNewPatient.Location = new System.Drawing.Point(339, 9);
            this.btnNewPatient.Name = "btnNewPatient";
            this.btnNewPatient.Size = new System.Drawing.Size(87, 23);
            this.btnNewPatient.TabIndex = 4;
            this.btnNewPatient.Text = "New Patient";
            this.btnNewPatient.UseVisualStyleBackColor = true;
            this.btnNewPatient.Click += new System.EventHandler(this.btnNewPatient_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.AutoSize = true;
            this.btnSearch.Location = new System.Drawing.Point(246, 9);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(87, 23);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(370, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Visit Date/Time:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Patient Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Patient ID:";
            // 
            // tabControlPatient
            // 
            this.tabControlPatient.Controls.Add(this.tabRegistration);
            this.tabControlPatient.Controls.Add(this.tabVisit);
            this.tabControlPatient.Controls.Add(this.tabDiagnosis);
            this.tabControlPatient.Controls.Add(this.tabTests);
            this.tabControlPatient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPatient.Location = new System.Drawing.Point(0, 0);
            this.tabControlPatient.Name = "tabControlPatient";
            this.tabControlPatient.SelectedIndex = 0;
            this.tabControlPatient.Size = new System.Drawing.Size(701, 381);
            this.tabControlPatient.TabIndex = 1;
            this.tabControlPatient.TabStop = false;
            // 
            // tabRegistration
            // 
            this.tabRegistration.AutoScroll = true;
            this.tabRegistration.Controls.Add(this.sSNMaskedTextBox);
            this.tabRegistration.Controls.Add(this.stateCodeComboBox);
            this.tabRegistration.Controls.Add(this.genderComboBox);
            this.tabRegistration.Controls.Add(this.phoneTextBox);
            this.tabRegistration.Controls.Add(this.zipTextBox);
            this.tabRegistration.Controls.Add(this.cityTextBox);
            this.tabRegistration.Controls.Add(this.address2TextBox);
            this.tabRegistration.Controls.Add(this.address1TextBox);
            this.tabRegistration.Controls.Add(this.dOBDateTimePicker);
            this.tabRegistration.Controls.Add(this.lastNameTextBox);
            this.tabRegistration.Controls.Add(this.middleInitialTextBox);
            this.tabRegistration.Controls.Add(firstNameLabel);
            this.tabRegistration.Controls.Add(this.firstNameTextBox);
            this.tabRegistration.Controls.Add(lblState);
            this.tabRegistration.Controls.Add(this.label33);
            this.tabRegistration.Controls.Add(this.btnRegistrationCancel);
            this.tabRegistration.Controls.Add(this.btnRegistrationSave);
            this.tabRegistration.Controls.Add(this.label20);
            this.tabRegistration.Controls.Add(this.label19);
            this.tabRegistration.Controls.Add(this.label18);
            this.tabRegistration.Controls.Add(this.label17);
            this.tabRegistration.Controls.Add(this.label15);
            this.tabRegistration.Controls.Add(this.label14);
            this.tabRegistration.Controls.Add(this.label13);
            this.tabRegistration.Controls.Add(this.label12);
            this.tabRegistration.Controls.Add(this.label11);
            this.tabRegistration.Controls.Add(this.label10);
            this.tabRegistration.Location = new System.Drawing.Point(4, 22);
            this.tabRegistration.Name = "tabRegistration";
            this.tabRegistration.Size = new System.Drawing.Size(693, 355);
            this.tabRegistration.TabIndex = 3;
            this.tabRegistration.Text = "Registration";
            this.tabRegistration.UseVisualStyleBackColor = true;
            // 
            // sSNMaskedTextBox
            // 
            this.sSNMaskedTextBox.CausesValidation = false;
            this.sSNMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "SSN", true));
            this.sSNMaskedTextBox.Location = new System.Drawing.Point(85, 65);
            this.sSNMaskedTextBox.Mask = "000-00-0000";
            this.sSNMaskedTextBox.Name = "sSNMaskedTextBox";
            this.sSNMaskedTextBox.Size = new System.Drawing.Size(68, 20);
            this.sSNMaskedTextBox.TabIndex = 29;
            this.sSNMaskedTextBox.Tag = "SSN";
            this.sSNMaskedTextBox.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // stateCodeComboBox
            // 
            this.stateCodeComboBox.CausesValidation = false;
            this.stateCodeComboBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.stateCodeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "StateCode", true));
            this.stateCodeComboBox.DataSource = this.stateBindingSource;
            this.stateCodeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateCodeComboBox.Enabled = false;
            this.stateCodeComboBox.FormattingEnabled = true;
            this.stateCodeComboBox.Location = new System.Drawing.Point(398, 197);
            this.stateCodeComboBox.Name = "stateCodeComboBox";
            this.stateCodeComboBox.Size = new System.Drawing.Size(49, 21);
            this.stateCodeComboBox.TabIndex = 19;
            this.stateCodeComboBox.Tag = "State";
            this.stateCodeComboBox.SelectionChangeCommitted += new System.EventHandler(this.stateCodeComboBox_SelectionChangeCommitted);
            // 
            // stateBindingSource
            // 
            this.stateBindingSource.DataSource = typeof(Clinicator.Model.State);
            // 
            // genderComboBox
            // 
            this.genderComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "M",
            "F"});
            this.genderComboBox.CausesValidation = false;
            this.genderComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "Gender", true));
            this.genderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.genderComboBox.Enabled = false;
            this.genderComboBox.FormattingEnabled = true;
            this.genderComboBox.Location = new System.Drawing.Point(498, 36);
            this.genderComboBox.Name = "genderComboBox";
            this.genderComboBox.Size = new System.Drawing.Size(43, 21);
            this.genderComboBox.TabIndex = 13;
            this.genderComboBox.Tag = "Gender";
            this.genderComboBox.SelectionChangeCommitted += new System.EventHandler(this.genderComboBox_SelectionChangeCommitted);
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.CausesValidation = false;
            this.phoneTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "Phone", true));
            this.phoneTextBox.Enabled = false;
            this.phoneTextBox.Location = new System.Drawing.Point(85, 223);
            this.phoneTextBox.MaxLength = 50;
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.Size = new System.Drawing.Size(151, 20);
            this.phoneTextBox.TabIndex = 21;
            this.phoneTextBox.Tag = "Phone";
            // 
            // zipTextBox
            // 
            this.zipTextBox.CausesValidation = false;
            this.zipTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "Zip", true));
            this.zipTextBox.Enabled = false;
            this.zipTextBox.Location = new System.Drawing.Point(479, 197);
            this.zipTextBox.MaxLength = 5;
            this.zipTextBox.Name = "zipTextBox";
            this.zipTextBox.Size = new System.Drawing.Size(62, 20);
            this.zipTextBox.TabIndex = 20;
            this.zipTextBox.Tag = "Zip Code";
            // 
            // cityTextBox
            // 
            this.cityTextBox.CausesValidation = false;
            this.cityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "City", true));
            this.cityTextBox.Enabled = false;
            this.cityTextBox.Location = new System.Drawing.Point(85, 197);
            this.cityTextBox.MaxLength = 50;
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(254, 20);
            this.cityTextBox.TabIndex = 18;
            this.cityTextBox.Tag = "City";
            // 
            // address2TextBox
            // 
            this.address2TextBox.CausesValidation = false;
            this.address2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "Address2", true));
            this.address2TextBox.Enabled = false;
            this.address2TextBox.Location = new System.Drawing.Point(85, 165);
            this.address2TextBox.MaxLength = 50;
            this.address2TextBox.Name = "address2TextBox";
            this.address2TextBox.Size = new System.Drawing.Size(254, 20);
            this.address2TextBox.TabIndex = 17;
            // 
            // address1TextBox
            // 
            this.address1TextBox.CausesValidation = false;
            this.address1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "Address1", true));
            this.address1TextBox.Enabled = false;
            this.address1TextBox.Location = new System.Drawing.Point(85, 133);
            this.address1TextBox.MaxLength = 50;
            this.address1TextBox.Name = "address1TextBox";
            this.address1TextBox.Size = new System.Drawing.Size(254, 20);
            this.address1TextBox.TabIndex = 16;
            this.address1TextBox.Tag = "Address 1";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.CausesValidation = false;
            this.lastNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "LastName", true));
            this.lastNameTextBox.Enabled = false;
            this.lastNameTextBox.Location = new System.Drawing.Point(286, 37);
            this.lastNameTextBox.MaxLength = 50;
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(164, 20);
            this.lastNameTextBox.TabIndex = 12;
            this.lastNameTextBox.Tag = "Last Name";
            // 
            // middleInitialTextBox
            // 
            this.middleInitialTextBox.CausesValidation = false;
            this.middleInitialTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.middleInitialTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "MiddleInitial", true));
            this.middleInitialTextBox.Enabled = false;
            this.middleInitialTextBox.Location = new System.Drawing.Point(250, 36);
            this.middleInitialTextBox.MaxLength = 1;
            this.middleInitialTextBox.Name = "middleInitialTextBox";
            this.middleInitialTextBox.Size = new System.Drawing.Size(30, 20);
            this.middleInitialTextBox.TabIndex = 11;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.CausesValidation = false;
            this.firstNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.patientBindingSource, "FirstName", true));
            this.firstNameTextBox.Enabled = false;
            this.firstNameTextBox.Location = new System.Drawing.Point(85, 36);
            this.firstNameTextBox.MaxLength = 50;
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(151, 20);
            this.firstNameTextBox.TabIndex = 10;
            this.firstNameTextBox.Tag = "First Name";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(38, 226);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 13);
            this.label33.TabIndex = 25;
            this.label33.Text = "Phone:";
            // 
            // btnRegistrationCancel
            // 
            this.btnRegistrationCancel.Location = new System.Drawing.Point(580, 249);
            this.btnRegistrationCancel.Name = "btnRegistrationCancel";
            this.btnRegistrationCancel.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrationCancel.TabIndex = 24;
            this.btnRegistrationCancel.Text = "Cancel";
            this.btnRegistrationCancel.UseVisualStyleBackColor = true;
            this.btnRegistrationCancel.Click += new System.EventHandler(this.btnRegistrationCancel_Click);
            // 
            // btnRegistrationSave
            // 
            this.btnRegistrationSave.AutoSize = true;
            this.btnRegistrationSave.Location = new System.Drawing.Point(473, 249);
            this.btnRegistrationSave.Name = "btnRegistrationSave";
            this.btnRegistrationSave.Size = new System.Drawing.Size(101, 23);
            this.btnRegistrationSave.TabIndex = 22;
            this.btnRegistrationSave.Text = "Save Registration";
            this.btnRegistrationSave.UseVisualStyleBackColor = true;
            this.btnRegistrationSave.Click += new System.EventHandler(this.btnRegistrationSave_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.CausesValidation = false;
            this.label20.Location = new System.Drawing.Point(10, 104);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "Date of Birth:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.CausesValidation = false;
            this.label19.Location = new System.Drawing.Point(47, 72);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "SSN:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.CausesValidation = false;
            this.label18.Location = new System.Drawing.Point(456, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "Gender:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.CausesValidation = false;
            this.label17.Location = new System.Drawing.Point(453, 200);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(25, 13);
            this.label17.TabIndex = 15;
            this.label17.Text = "Zip:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.CausesValidation = false;
            this.label15.Location = new System.Drawing.Point(52, 200);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "City:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.CausesValidation = false;
            this.label14.Location = new System.Drawing.Point(22, 168);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Address 2:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.CausesValidation = false;
            this.label13.Location = new System.Drawing.Point(22, 136);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Address 1:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.CausesValidation = false;
            this.label12.Location = new System.Drawing.Point(283, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Last";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.CausesValidation = false;
            this.label11.Location = new System.Drawing.Point(258, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "MI";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.CausesValidation = false;
            this.label10.Location = new System.Drawing.Point(82, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "First";
            // 
            // tabVisit
            // 
            this.tabVisit.AutoScroll = true;
            this.tabVisit.Controls.Add(this.doctorIDComboBox);
            this.tabVisit.Controls.Add(symptomsLabel);
            this.tabVisit.Controls.Add(this.symptomsTextBox);
            this.tabVisit.Controls.Add(this.label28);
            this.tabVisit.Controls.Add(this.txtNurseName);
            this.tabVisit.Controls.Add(this.label29);
            this.tabVisit.Controls.Add(this.groupBox2);
            this.tabVisit.Controls.Add(this.btnCancelVisit);
            this.tabVisit.Controls.Add(this.btnAddVisit);
            this.tabVisit.Location = new System.Drawing.Point(4, 22);
            this.tabVisit.Name = "tabVisit";
            this.tabVisit.Padding = new System.Windows.Forms.Padding(3);
            this.tabVisit.Size = new System.Drawing.Size(693, 355);
            this.tabVisit.TabIndex = 2;
            this.tabVisit.Text = "Visit";
            this.tabVisit.UseVisualStyleBackColor = true;
            // 
            // doctorIDComboBox
            // 
            this.doctorIDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "DoctorID", true));
            this.doctorIDComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.doctorIDComboBox.FormattingEnabled = true;
            this.doctorIDComboBox.Location = new System.Drawing.Point(311, 169);
            this.doctorIDComboBox.Name = "doctorIDComboBox";
            this.doctorIDComboBox.Size = new System.Drawing.Size(186, 21);
            this.doctorIDComboBox.TabIndex = 48;
            this.doctorIDComboBox.Tag = "Doctor";
            // 
            // symptomsTextBox
            // 
            this.symptomsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "Symptoms", true));
            this.symptomsTextBox.Location = new System.Drawing.Point(311, 25);
            this.symptomsTextBox.MaxLength = 1000;
            this.symptomsTextBox.Multiline = true;
            this.symptomsTextBox.Name = "symptomsTextBox";
            this.symptomsTextBox.Size = new System.Drawing.Size(294, 76);
            this.symptomsTextBox.TabIndex = 46;
            this.symptomsTextBox.Tag = "Symptoms";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(263, 172);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(42, 13);
            this.label28.TabIndex = 42;
            this.label28.Text = "Doctor:";
            // 
            // txtNurseName
            // 
            this.txtNurseName.Location = new System.Drawing.Point(311, 136);
            this.txtNurseName.Name = "txtNurseName";
            this.txtNurseName.ReadOnly = true;
            this.txtNurseName.Size = new System.Drawing.Size(186, 20);
            this.txtNurseName.TabIndex = 47;
            this.txtNurseName.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(269, 139);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(38, 13);
            this.label29.TabIndex = 40;
            this.label29.Text = "Nurse:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pulseTextBox);
            this.groupBox2.Controls.Add(temperatureLabel);
            this.groupBox2.Controls.Add(this.temperatureTextBox);
            this.groupBox2.Controls.Add(pulseLabel);
            this.groupBox2.Controls.Add(weightLabel);
            this.groupBox2.Controls.Add(this.weightTextBox);
            this.groupBox2.Controls.Add(heightLabel);
            this.groupBox2.Controls.Add(this.heightTextBox);
            this.groupBox2.Controls.Add(bloodPressureSystolicLabel);
            this.groupBox2.Controls.Add(this.bloodPressureSystolicTextBox);
            this.groupBox2.Controls.Add(bloodPressureDiastolicLabel);
            this.groupBox2.Controls.Add(this.bloodPressureDiastolicTextBox);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(228, 211);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vitals";
            // 
            // pulseTextBox
            // 
            this.pulseTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "Pulse", true));
            this.pulseTextBox.Location = new System.Drawing.Point(88, 130);
            this.pulseTextBox.MaxLength = 8;
            this.pulseTextBox.Name = "pulseTextBox";
            this.pulseTextBox.Size = new System.Drawing.Size(114, 20);
            this.pulseTextBox.TabIndex = 44;
            this.pulseTextBox.Tag = "Pulse";
            // 
            // temperatureTextBox
            // 
            this.temperatureTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "Temperature", true));
            this.temperatureTextBox.Location = new System.Drawing.Point(88, 166);
            this.temperatureTextBox.MaxLength = 6;
            this.temperatureTextBox.Name = "temperatureTextBox";
            this.temperatureTextBox.Size = new System.Drawing.Size(47, 20);
            this.temperatureTextBox.TabIndex = 45;
            this.temperatureTextBox.Tag = "Temperature";
            // 
            // weightTextBox
            // 
            this.weightTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "Weight", true));
            this.weightTextBox.Location = new System.Drawing.Point(88, 52);
            this.weightTextBox.MaxLength = 7;
            this.weightTextBox.Name = "weightTextBox";
            this.weightTextBox.Size = new System.Drawing.Size(47, 20);
            this.weightTextBox.TabIndex = 41;
            this.weightTextBox.Tag = "Weight";
            // 
            // heightTextBox
            // 
            this.heightTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "Height", true));
            this.heightTextBox.Location = new System.Drawing.Point(88, 19);
            this.heightTextBox.MaxLength = 8;
            this.heightTextBox.Name = "heightTextBox";
            this.heightTextBox.Size = new System.Drawing.Size(47, 20);
            this.heightTextBox.TabIndex = 40;
            this.heightTextBox.Tag = "Height";
            // 
            // bloodPressureSystolicTextBox
            // 
            this.bloodPressureSystolicTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "BloodPressureSystolic", true));
            this.bloodPressureSystolicTextBox.Location = new System.Drawing.Point(88, 91);
            this.bloodPressureSystolicTextBox.MaxLength = 3;
            this.bloodPressureSystolicTextBox.Name = "bloodPressureSystolicTextBox";
            this.bloodPressureSystolicTextBox.Size = new System.Drawing.Size(38, 20);
            this.bloodPressureSystolicTextBox.TabIndex = 42;
            this.bloodPressureSystolicTextBox.Tag = "Systolic Pressure";
            // 
            // bloodPressureDiastolicTextBox
            // 
            this.bloodPressureDiastolicTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "BloodPressureDiastolic", true));
            this.bloodPressureDiastolicTextBox.Location = new System.Drawing.Point(164, 91);
            this.bloodPressureDiastolicTextBox.MaxLength = 3;
            this.bloodPressureDiastolicTextBox.Name = "bloodPressureDiastolicTextBox";
            this.bloodPressureDiastolicTextBox.Size = new System.Drawing.Size(38, 20);
            this.bloodPressureDiastolicTextBox.TabIndex = 43;
            this.bloodPressureDiastolicTextBox.Tag = "Diastolic Pressure";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(138, 58);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(20, 13);
            this.label32.TabIndex = 38;
            this.label32.Text = "lbs";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(138, 22);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(38, 13);
            this.label31.TabIndex = 37;
            this.label31.Text = "inches";
            // 
            // btnCancelVisit
            // 
            this.btnCancelVisit.Location = new System.Drawing.Point(530, 219);
            this.btnCancelVisit.Name = "btnCancelVisit";
            this.btnCancelVisit.Size = new System.Drawing.Size(75, 23);
            this.btnCancelVisit.TabIndex = 50;
            this.btnCancelVisit.Text = "Cancel";
            this.btnCancelVisit.UseVisualStyleBackColor = true;
            this.btnCancelVisit.Click += new System.EventHandler(this.btnCancelVisit_Click);
            // 
            // btnAddVisit
            // 
            this.btnAddVisit.Location = new System.Drawing.Point(451, 219);
            this.btnAddVisit.Name = "btnAddVisit";
            this.btnAddVisit.Size = new System.Drawing.Size(75, 23);
            this.btnAddVisit.TabIndex = 49;
            this.btnAddVisit.Text = "Add Visit";
            this.btnAddVisit.UseVisualStyleBackColor = true;
            this.btnAddVisit.Click += new System.EventHandler(this.btnAddVisit_Click);
            // 
            // tabDiagnosis
            // 
            this.tabDiagnosis.AutoScroll = true;
            this.tabDiagnosis.Controls.Add(initialDiagnosisLabel);
            this.tabDiagnosis.Controls.Add(this.initialDiagnosisTextBox);
            this.tabDiagnosis.Controls.Add(this.btnDiagnosisCancel);
            this.tabDiagnosis.Controls.Add(this.btnDiagnosisSave);
            this.tabDiagnosis.Controls.Add(this.groupBox1);
            this.tabDiagnosis.Location = new System.Drawing.Point(4, 22);
            this.tabDiagnosis.Name = "tabDiagnosis";
            this.tabDiagnosis.Padding = new System.Windows.Forms.Padding(3);
            this.tabDiagnosis.Size = new System.Drawing.Size(693, 355);
            this.tabDiagnosis.TabIndex = 0;
            this.tabDiagnosis.Text = "Diagnosis";
            this.tabDiagnosis.UseVisualStyleBackColor = true;
            // 
            // initialDiagnosisTextBox
            // 
            this.initialDiagnosisTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "InitialDiagnosis", true));
            this.initialDiagnosisTextBox.Location = new System.Drawing.Point(95, 16);
            this.initialDiagnosisTextBox.Multiline = true;
            this.initialDiagnosisTextBox.Name = "initialDiagnosisTextBox";
            this.initialDiagnosisTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.initialDiagnosisTextBox.Size = new System.Drawing.Size(439, 70);
            this.initialDiagnosisTextBox.TabIndex = 28;
            this.initialDiagnosisTextBox.Tag = "Initial Diagnosis";
            // 
            // btnDiagnosisCancel
            // 
            this.btnDiagnosisCancel.Location = new System.Drawing.Point(550, 240);
            this.btnDiagnosisCancel.Name = "btnDiagnosisCancel";
            this.btnDiagnosisCancel.Size = new System.Drawing.Size(75, 23);
            this.btnDiagnosisCancel.TabIndex = 27;
            this.btnDiagnosisCancel.Text = "Cancel";
            this.btnDiagnosisCancel.UseVisualStyleBackColor = true;
            this.btnDiagnosisCancel.Click += new System.EventHandler(this.btnDiagnosisCancel_Click);
            // 
            // btnDiagnosisSave
            // 
            this.btnDiagnosisSave.Location = new System.Drawing.Point(459, 240);
            this.btnDiagnosisSave.Name = "btnDiagnosisSave";
            this.btnDiagnosisSave.Size = new System.Drawing.Size(75, 23);
            this.btnDiagnosisSave.TabIndex = 26;
            this.btnDiagnosisSave.Text = "Save";
            this.btnDiagnosisSave.UseVisualStyleBackColor = true;
            this.btnDiagnosisSave.Click += new System.EventHandler(this.btnDiagnosisSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.finalDiagnosisDateTextBox);
            this.groupBox1.Controls.Add(finalDiagnosisLabel);
            this.groupBox1.Controls.Add(this.finalDiagnosisTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(9, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(549, 142);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Final Diagnosis";
            // 
            // finalDiagnosisDateTextBox
            // 
            this.finalDiagnosisDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "FinalDiagnosisDate", true));
            this.finalDiagnosisDateTextBox.Enabled = false;
            this.finalDiagnosisDateTextBox.Location = new System.Drawing.Point(86, 101);
            this.finalDiagnosisDateTextBox.Name = "finalDiagnosisDateTextBox";
            this.finalDiagnosisDateTextBox.ReadOnly = true;
            this.finalDiagnosisDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.finalDiagnosisDateTextBox.TabIndex = 29;
            // 
            // finalDiagnosisTextBox
            // 
            this.finalDiagnosisTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.visitBindingSource, "FinalDiagnosis", true));
            this.finalDiagnosisTextBox.Location = new System.Drawing.Point(86, 25);
            this.finalDiagnosisTextBox.Multiline = true;
            this.finalDiagnosisTextBox.Name = "finalDiagnosisTextBox";
            this.finalDiagnosisTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.finalDiagnosisTextBox.Size = new System.Drawing.Size(439, 70);
            this.finalDiagnosisTextBox.TabIndex = 28;
            this.finalDiagnosisTextBox.Tag = "Final Diagnosis Description";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Date:";
            // 
            // tabTests
            // 
            this.tabTests.AutoScroll = true;
            this.tabTests.Controls.Add(this.testNameComboBox);
            this.tabTests.Controls.Add(this.groupBox3);
            this.tabTests.Controls.Add(this.label9);
            this.tabTests.Controls.Add(this.label8);
            this.tabTests.Controls.Add(this.btnOrder);
            this.tabTests.Controls.Add(this.dgvTestOrders);
            this.tabTests.Location = new System.Drawing.Point(4, 22);
            this.tabTests.Name = "tabTests";
            this.tabTests.Padding = new System.Windows.Forms.Padding(3);
            this.tabTests.Size = new System.Drawing.Size(693, 355);
            this.tabTests.TabIndex = 1;
            this.tabTests.Text = "Tests";
            this.tabTests.UseVisualStyleBackColor = true;
            // 
            // testNameComboBox
            // 
            this.testNameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.testNameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.testNameComboBox.FormattingEnabled = true;
            this.testNameComboBox.Location = new System.Drawing.Point(67, 248);
            this.testNameComboBox.Name = "testNameComboBox";
            this.testNameComboBox.Size = new System.Drawing.Size(158, 21);
            this.testNameComboBox.TabIndex = 38;
            this.testNameComboBox.SelectionChangeCommitted += new System.EventHandler(this.testNameComboBox_SelectionChangeCommitted);
            this.testNameComboBox.SelectedValueChanged += new System.EventHandler(this.testNameComboBox_SelectedValueChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cboResults);
            this.groupBox3.Controls.Add(this.btnUpdateResults);
            this.groupBox3.Controls.Add(this.dtpDatePerformed);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Location = new System.Drawing.Point(362, 246);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(323, 65);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Selected Test";
            // 
            // cboResults
            // 
            this.cboResults.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboResults.FormattingEnabled = true;
            this.cboResults.Items.AddRange(new object[] {
            "Positive",
            "Negative"});
            this.cboResults.Location = new System.Drawing.Point(96, 12);
            this.cboResults.Name = "cboResults";
            this.cboResults.Size = new System.Drawing.Size(100, 21);
            this.cboResults.TabIndex = 32;
            // 
            // btnUpdateResults
            // 
            this.btnUpdateResults.AutoSize = true;
            this.btnUpdateResults.Location = new System.Drawing.Point(216, 11);
            this.btnUpdateResults.Name = "btnUpdateResults";
            this.btnUpdateResults.Size = new System.Drawing.Size(90, 23);
            this.btnUpdateResults.TabIndex = 34;
            this.btnUpdateResults.Text = "Update Results";
            this.btnUpdateResults.UseVisualStyleBackColor = true;
            this.btnUpdateResults.Click += new System.EventHandler(this.btnUpdateResults_Click);
            // 
            // dtpDatePerformed
            // 
            this.dtpDatePerformed.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDatePerformed.Location = new System.Drawing.Point(96, 39);
            this.dtpDatePerformed.Name = "dtpDatePerformed";
            this.dtpDatePerformed.ShowCheckBox = true;
            this.dtpDatePerformed.Size = new System.Drawing.Size(100, 20);
            this.dtpDatePerformed.TabIndex = 35;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 45);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(84, 13);
            this.label30.TabIndex = 36;
            this.label30.Text = "Date Performed:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(50, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(40, 13);
            this.label21.TabIndex = 33;
            this.label21.Text = "Result:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 251);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Add Test:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Currently Ordered Tests";
            // 
            // btnOrder
            // 
            this.btnOrder.Location = new System.Drawing.Point(231, 246);
            this.btnOrder.Name = "btnOrder";
            this.btnOrder.Size = new System.Drawing.Size(75, 23);
            this.btnOrder.TabIndex = 28;
            this.btnOrder.Text = "Order Test";
            this.btnOrder.UseVisualStyleBackColor = true;
            this.btnOrder.Click += new System.EventHandler(this.btnOrder_Click);
            // 
            // dgvTestOrders
            // 
            this.dgvTestOrders.AllowUserToAddRows = false;
            this.dgvTestOrders.AllowUserToDeleteRows = false;
            this.dgvTestOrders.AutoGenerateColumns = false;
            this.dgvTestOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTestOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.btnTestOrderRemove,
            this.visitIDDataGridViewTextBoxColumn,
            this.testIDDataGridViewTextBoxColumn});
            this.dgvTestOrders.DataSource = this.testOrderBindingSource;
            this.dgvTestOrders.Location = new System.Drawing.Point(6, 29);
            this.dgvTestOrders.MultiSelect = false;
            this.dgvTestOrders.Name = "dgvTestOrders";
            this.dgvTestOrders.ReadOnly = true;
            this.dgvTestOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTestOrders.ShowEditingIcon = false;
            this.dgvTestOrders.Size = new System.Drawing.Size(679, 205);
            this.dgvTestOrders.TabIndex = 0;
            this.dgvTestOrders.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTestOrders_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "TestName";
            this.Column1.HeaderText = "Test";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.Width = 53;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column2.DataPropertyName = "DatePerformed";
            this.Column2.HeaderText = "Date Performed";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 97;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.DataPropertyName = "Results";
            this.Column3.HeaderText = "Results";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // btnTestOrderRemove
            // 
            this.btnTestOrderRemove.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.btnTestOrderRemove.HeaderText = "";
            this.btnTestOrderRemove.Name = "btnTestOrderRemove";
            this.btnTestOrderRemove.ReadOnly = true;
            this.btnTestOrderRemove.Text = "Remove";
            this.btnTestOrderRemove.UseColumnTextForButtonValue = true;
            this.btnTestOrderRemove.Width = 5;
            // 
            // visitIDDataGridViewTextBoxColumn
            // 
            this.visitIDDataGridViewTextBoxColumn.DataPropertyName = "VisitID";
            this.visitIDDataGridViewTextBoxColumn.HeaderText = "VisitID";
            this.visitIDDataGridViewTextBoxColumn.Name = "visitIDDataGridViewTextBoxColumn";
            this.visitIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.visitIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // testIDDataGridViewTextBoxColumn
            // 
            this.testIDDataGridViewTextBoxColumn.DataPropertyName = "TestID";
            this.testIDDataGridViewTextBoxColumn.HeaderText = "TestID";
            this.testIDDataGridViewTextBoxColumn.Name = "testIDDataGridViewTextBoxColumn";
            this.testIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.testIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // testOrderBindingSource
            // 
            this.testOrderBindingSource.DataSource = typeof(Clinicator.Model.TestOrder);
            // 
            // frmPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(701, 475);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPatient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Patient";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.handlePatientSearchResults);
            this.Load += new System.EventHandler(this.frmPatient_Load);
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.visitBindingSource)).EndInit();
            this.tabControlPatient.ResumeLayout(false);
            this.tabRegistration.ResumeLayout(false);
            this.tabRegistration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).EndInit();
            this.tabVisit.ResumeLayout(false);
            this.tabVisit.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabDiagnosis.ResumeLayout(false);
            this.tabDiagnosis.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabTests.ResumeLayout(false);
            this.tabTests.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTestOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testOrderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControlPatient;
        private System.Windows.Forms.TabPage tabRegistration;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabVisit;
        private System.Windows.Forms.TabPage tabDiagnosis;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabTests;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnOrder;
        private System.Windows.Forms.DataGridView dgvTestOrders;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRegistrationCancel;
        private System.Windows.Forms.Button btnRegistrationSave;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnAddVisit;
        private System.Windows.Forms.Button btnDiagnosisCancel;
        private System.Windows.Forms.Button btnDiagnosisSave;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cboResults;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtNurseName;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCancelVisit;
        private System.Windows.Forms.Button btnUpdateResults;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker dtpDatePerformed;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnNewPatient;
        private System.Windows.Forms.Button btnRetrieve;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.BindingSource stateBindingSource;
        private System.Windows.Forms.BindingSource patientBindingSource;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox middleInitialTextBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.TextBox address2TextBox;
        private System.Windows.Forms.TextBox address1TextBox;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.TextBox zipTextBox;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.TextBox iDTextBox;
        private System.Windows.Forms.ComboBox genderComboBox;
        private System.Windows.Forms.TextBox patientNameTextBox;
        private System.Windows.Forms.ComboBox visitComboBox;
        private System.Windows.Forms.ComboBox stateCodeComboBox;
        private System.Windows.Forms.BindingSource visitBindingSource;
        private System.Windows.Forms.DateTimePicker dOBDateTimePicker;
        private System.Windows.Forms.Button btnNewVisit;
        private System.Windows.Forms.TextBox symptomsTextBox;
        private System.Windows.Forms.TextBox temperatureTextBox;
        private System.Windows.Forms.TextBox weightTextBox;
        private System.Windows.Forms.TextBox heightTextBox;
        private System.Windows.Forms.TextBox bloodPressureSystolicTextBox;
        private System.Windows.Forms.TextBox bloodPressureDiastolicTextBox;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.ComboBox doctorIDComboBox;
        private System.Windows.Forms.TextBox pulseTextBox;
        private System.Windows.Forms.TextBox initialDiagnosisTextBox;
        private System.Windows.Forms.TextBox finalDiagnosisTextBox;
        private System.Windows.Forms.TextBox finalDiagnosisDateTextBox;
        private System.Windows.Forms.ComboBox testNameComboBox;
        private System.Windows.Forms.BindingSource testBindingSource;
        private System.Windows.Forms.BindingSource testOrderBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewButtonColumn btnTestOrderRemove;
        private System.Windows.Forms.DataGridViewTextBoxColumn visitIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn testIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.MaskedTextBox sSNMaskedTextBox;

    }
}